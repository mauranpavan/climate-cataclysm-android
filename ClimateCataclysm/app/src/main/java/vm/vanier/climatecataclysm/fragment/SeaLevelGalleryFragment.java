package vm.vanier.climatecataclysm.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import vm.vanier.climatecataclysm.R;

/**
 * Displays images related to Sea Level Rise
 * @author Mauran P
 * @version Version 1
 */
public class SeaLevelGalleryFragment extends Fragment {


    //empty constructor
    public SeaLevelGalleryFragment() {

    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_sea_level_gallery, container, false);

        ImageView image = (ImageView) view.findViewById(R.id.imageView);
        image.setImageResource(R.drawable.infographic_sealevel);



        return view;
    }

}
