package vm.vanier.climatecataclysm.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import vm.vanier.climatecataclysm.R;

/**
 * DialogFragment allows to gather two year inputs from the User to compare sea levels at two different occasion
 *
 * @author Mauran P.
 * @version Version 1
 *
 * References:
 * - onFocusChangeListener Example: https://www.youtube.com/watch?v=F1FYIB0_le4
 * - setError examples: https://www.programcreek.com/java-api-examples/?class=android.widget.TextView&method=setError
 * - Android Documentation: https://developer.android.com/reference/android/view/View.OnFocusChangeListener.html
 * - Android Documentation: https://developer.android.com/reference/android/widget/TextView.html#setError(java.lang.CharSequence)
 */
public class YearInputDialog extends DialogFragment {


    private EditText txtYearA;
    private EditText txtYearB;
    private EditText txtUserInput; //used for validating input
    private Button btnSubmit;
    private Button btnCancel;


    private final static String TAG = "YearInputDialog";
    private int yearA;
    private int yearB;
    private boolean inputFlag = false;
    private boolean yearAFlag = false;
    private boolean yearBFlag = false;

    public YearInputDialog() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_year_input_dialog, container, false);

        //initialize ui components
        txtYearA = (EditText) view.findViewById(R.id.txtYearA);
        txtYearB = (EditText) view.findViewById(R.id.txtYearB);
        btnSubmit = (Button) view.findViewById(R.id.btnSubmitYearInputDialog);
        btnCancel = (Button) view.findViewById(R.id.btnCancelYearInputDialog);

        //We want to keep the submit button disabled until all the inputs are valid
        btnSubmit.setEnabled(false);

        /* onFocusChangeListener */
        //By adding setOnFocusChangeListener we can keep track of where the User interacts between the two edittexts
        txtYearA.setOnFocusChangeListener(new View.OnFocusChangeListener() { // This is for the first EditText (txtYearA)
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (!txtYearA.hasFocus()) {
                    yearAFlag = validateUserInput(txtYearA);
                    Log.d(TAG, "onFocusChange A has focus" + yearAFlag);
                }

                //Only when both inputs are valid that the User will be able to click on submit
                if (yearAFlag == true && yearBFlag == true) {
                    btnSubmit.setEnabled(true);
                    Log.d(TAG, "onCreateView: both edittext are valid " + txtYearA.getText().toString() + " and " + txtYearB.getText().toString());
                }
            }
        });
        txtYearB.setOnFocusChangeListener(new View.OnFocusChangeListener() { // This is for the second EditText (txtYearB)
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (!txtYearB.hasFocus()) {
                    yearBFlag = validateUserInput(txtYearB);
                    Log.d(TAG, "onFocusChange: B has focus" + yearBFlag);
                }

                //Only when both inputs are valid that the User will be able to click on submit
                if (yearAFlag == true && yearBFlag == true) {
                    btnSubmit.setEnabled(true);
                    Log.d(TAG, "onCreateView: both edittext are valid " + txtYearA.getText().toString() + " and " + txtYearB.getText().toString());
                }

            }
        });


        /* onClickListener */
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                yearA = Integer.parseInt(txtYearA.getText().toString());
                yearB = Integer.parseInt(txtYearB.getText().toString());

                //Now we send the year values to SeaLevelRise Activity
                ISeaLevelDialogListener activity = (ISeaLevelDialogListener) getActivity();

                //We send the year values to SeaLevelRise Activity
                activity.onFinishYearInputDialog(yearA, yearB);

                //once we sent it, we can close the dialogfragment
                dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return view;
    }




    /**
     * Validates if the User input satisfies the defined condition for a valid year within the range of 1993 to 2015. Returns True if valid.
     * @param editText An EditText in which User writes their input
     * @return inputFlag
     */
    public boolean validateUserInput( EditText editText){  //We created this method to avoid code redundancy since we have to validate two edittexts

        txtUserInput = editText;

                int year;

                try {

                    if (!txtUserInput.getText().toString().isEmpty()) { //when they are not empty fields

                        //We retrieve the user inputs from the edittext as string, then parse it into an integer
                        year = Integer.parseInt(txtUserInput.getText().toString());

                        Log.d(TAG, "onFocusChange: Year:" +year);

                        //We validate if we have the correct input type
                        if (( year >= 1993 && year <= 2015)) {
                            inputFlag = true;
                            Log.d(TAG, "onFocusChange: Year is within range.");
                        }
                        else {
                            throw new Exception("Year value must be within 1993 and 2015");
                        }
                    }
                    else {
                        txtUserInput.setError("You did not enter any value.");
                    }


                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    Log.d(TAG, "validateUserInput:  NumberFormatException "  + e.getMessage());
                    txtUserInput.setError("Please Enter a number");
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d(TAG, "validateUserInput:  OutOfYearRange "  + e.getMessage());
                    txtUserInput.setError("Please Enter a value between 1993 and 2015");
                }



        Log.d(TAG, "validateUserInput:  inputFlag:" + inputFlag);
        return inputFlag;
    }

}
