package vm.vanier.climatecataclysm.activities;


import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.Locale;

import vm.vanier.climatecataclysm.R;


/**
 * Serves as the Main Home Page once app is initialized. Provides access to climate aspects such as Temperature, Precipitation, Carbon Emission, Ozone Layer and Sea Level Rise.
 *
 * @author Mauran P.
 * @version Version 1
 *
 */
public class ClimateAspects extends AppCompatActivity {

    private final static String TAG = "ClimateAspects";
    private Button btnSelect;
    private Intent intent;
    private SharedPreferences sharedPreferences;
    private  SharedPreferences.Editor sharedEditor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_climate_aspects);

        //get Settings shared preferences
        sharedPreferences = getSharedPreferences("Settings",MODE_PRIVATE);
        sharedEditor = sharedPreferences.edit();  //we will update user preferences in the option menu

        //Initialize UI elements
        btnSelect = (Button) findViewById(R.id.btnSelectClimateAspects);

        /*
        Locale locale = new Locale(sharedPreferences.getString("Language","ENGLISH").toLowerCase().substring(0,2)) ;
        Locale.setDefault(locale);
        Resources resources = getBaseContext().getResources();

        Configuration configuration = resources.getConfiguration();
        configuration.setLocale(locale); //since configuration.locale is deprecated, we use setLocale
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        */

        registerForContextMenu(btnSelect);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.climate_option_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.opMenuItemNews:
                Toast.makeText(this, "News menu item clicked", Toast.LENGTH_SHORT).show();
                intent = new Intent(this, ClimateNews.class);
                startActivity(intent);
                return true;
            case R.id.opMenuItemSpreadAwareness:
                Toast.makeText(this, "Spread Awareness menu item clicked", Toast.LENGTH_SHORT).show();
                intent = new Intent(this, SpreadAwareness.class);
                startActivity(intent);
                return true;
            case R.id.opMenuItemNotification:
                Toast.makeText(this, "Notifications clicked", Toast.LENGTH_SHORT).show();
                intent = new Intent(this, Notifications.class);
                startActivity(intent);
                return true;
            case R.id.opMenuItemLanguage:
                Toast.makeText(this, "Language clicked", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.opMenuItemEnglish:
                Toast.makeText(this, "English clicked", Toast.LENGTH_SHORT).show();
                sharedEditor.putString("Language", "English");
                sharedEditor.apply();
                intent = new Intent(this, StartScreen.class);
                startActivity(intent);
                return true;
            case R.id.opMenuItemFrench:
                Toast.makeText(this, "French clicked", Toast.LENGTH_SHORT).show();
                sharedEditor.putString("Language", "French");
                sharedEditor.apply();
                intent = new Intent(this, StartScreen.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }


    @Override
    /**
     * When a long-click event occurs in the registered view, the system calls this method
     */
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.setHeaderTitle("Climate Aspects");

        getMenuInflater().inflate(R.menu.climate_aspects_menu, menu);


    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {


        switch(item.getItemId()){
            case R.id.menuItemTemperature:
                Toast.makeText(this, item.getTitle(),Toast.LENGTH_SHORT).show();
                intent = new Intent(this, Temperature.class );
                startActivity(intent);
                return true;
            case R.id.menuItemPrecipitation:
                Toast.makeText(this, item.getTitle(),Toast.LENGTH_SHORT).show();
                intent = new Intent(this, Precipitation.class );
                startActivity(intent);
                return true;
            case R.id.menuItemCarbonEmission:
                Toast.makeText(this, item.getTitle(),Toast.LENGTH_SHORT).show();
                intent = new Intent(this, CarbonEmission.class );
                startActivity(intent);
                return true;
            case R.id.menuItemOzoneLayer:
                Toast.makeText(this, item.getTitle(),Toast.LENGTH_SHORT).show();
                intent = new Intent(this, OzoneLayer.class );
                startActivity(intent);
                return true;
            case R.id.menuItemSeaLevelRise:
                Toast.makeText(this, item.getTitle(),Toast.LENGTH_SHORT).show();
                intent = new Intent(this, SeaLevelRise.class );
                startActivity(intent);
                Log.d(TAG, "onContextItemSelected: sealevelrise");
                return true;
            case R.id.menuItemTemperatureList:
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
                intent = new Intent(this, TemperatureUser.class);
                startActivity(intent);
                Log.d(TAG, "onContextItemSelected: temperature");
                return true;
            default:
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
                return super.onContextItemSelected(item);
        }

    }
}
