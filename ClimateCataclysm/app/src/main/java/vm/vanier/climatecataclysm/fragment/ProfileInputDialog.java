package vm.vanier.climatecataclysm.fragment;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import vm.vanier.climatecataclysm.R;
import vm.vanier.climatecataclysm.model.UserProfile;

/**
 * Gathers User information for UserProfile and User preferences such as language selection.
 *
 * @author Mauran P.
 * @version Version 2
 */
public class ProfileInputDialog extends DialogFragment {


    private static String TAG = "ProfileInputDialog";
    private EditText txtUserName;
    private EditText txtUserAge;
    private EditText txtUserCountry;
    private RadioGroup radioGroupLanguage;
    private RadioButton radioBtnEnglish;
    private RadioButton radioBtnFrench;
    private Button btnSubmit;
    private Button btnCancel;


    public ProfileInputDialog() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile_input_dialog, container, false);

        //Initialize UI components
        txtUserName = view.findViewById(R.id.txtUserName);
        txtUserAge = view.findViewById(R.id.txtUserAge);
        txtUserCountry = view.findViewById(R.id.txtUserCountry);
        radioGroupLanguage = view.findViewById(R.id.radioGroupLanguage);
        radioBtnEnglish = view.findViewById(R.id.radioBtnEnglish);
        radioBtnFrench = view.findViewById(R.id.radioBtnFrench);
        btnSubmit = view.findViewById(R.id.btnSubmitProfileInput);
        btnCancel = view.findViewById(R.id.btnCancelProfileInput);

        verifySelectedOption();

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserProfile userProfile;
                String selectedLanguage="";


                int selectedBtnId = radioGroupLanguage.getCheckedRadioButtonId();

                switch (selectedBtnId){

                    case R.id.radioBtnEnglish:
                        Toast.makeText(getContext(), "Submitting " + radioBtnEnglish.getText().toString(), Toast.LENGTH_SHORT).show();
                        selectedLanguage = radioBtnEnglish.getText().toString();
                        break;

                    case R.id.radioBtnFrench:
                        Toast.makeText(getContext(), "Submitting " + radioBtnFrench.getText().toString(), Toast.LENGTH_SHORT).show();
                        selectedLanguage = radioBtnFrench.getText().toString();
                        break;


                }

                userProfile = new UserProfile(txtUserName.getText().toString(), Integer.parseInt(txtUserAge.getText().toString()), txtUserCountry.getText().toString());
                IProfileInputDialogListener activity = (IProfileInputDialogListener) getActivity();

                //passing the user information and language
                activity.onFinishProfileInputDialog(userProfile,selectedLanguage);

                Log.d(TAG, "onClick: profile " + userProfile + " language: " + selectedLanguage);
                dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        return view;
    }

    private void verifySelectedOption() {

        radioGroupLanguage.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId){
                    case R.id.radioBtnEnglish:
                        Toast.makeText(getContext(), "" + radioBtnEnglish.getText().toString(), Toast.LENGTH_SHORT).show();
                        break;

                    case R.id.radioBtnFrench:
                        Toast.makeText(getContext(), "" + radioBtnFrench.getText().toString(), Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
    }

}
