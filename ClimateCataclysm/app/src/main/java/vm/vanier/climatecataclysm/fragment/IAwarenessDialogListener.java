package vm.vanier.climatecataclysm.fragment;

import vm.vanier.climatecataclysm.model.AwarenessEntry;

/**
 * Interface associated with SpreadAwareness Activity
 *
 * @author Mauran P.
 * @version Version 1
 */
public interface IAwarenessDialogListener {

    void startAwarenessInputDialog();
    void onFinishAwarenessInputDialog(AwarenessEntry entry);
}
