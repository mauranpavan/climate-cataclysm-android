package vm.vanier.climatecataclysm.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;

import vm.vanier.climatecataclysm.R;

/**
 * Displays a graphical representation of Carbon Emission data for the selected country by the User
 *
 * @author Mauran P.
 * @version Version 3
 *
 * References:
 * - https://guides.codepath.com/android/Displaying-the-Snackbar
 * - https://developer.android.com/reference/android/os/Handler.html
 * - https://guides.codepath.com/android/Repeating-Periodic-Tasks
 */

public class CarbonEmissionDisplay extends AppCompatActivity {

    private LineGraphSeries<DataPoint> series;
    private Intent catchIntent;
    private String selectedCountry;
    private ArrayList<String> carbonEmissionList;
    private ArrayList<DataPoint> coordinatesList;
    private DataPoint[] coordinates;
    private TextView lblGraphTitle;
    private Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carbon_emission_display);

        //initialize
        coordinatesList = new ArrayList<>();
        lblGraphTitle = findViewById(R.id.lblCarbonEmissionTitle);

        //Retrieve the passed values from the intent
        catchIntent = getIntent();
        selectedCountry = catchIntent.getStringExtra("country");
        carbonEmissionList = catchIntent.getStringArrayListExtra("carbonEmissionList");

        //Creating a graph
        GraphView graph = findViewById(R.id.carbonEmissionGraph);
        double year = 2000;

        for(int i = 0; i < carbonEmissionList.size(); i++){
            DataPoint point = new DataPoint(year, Double.parseDouble(carbonEmissionList.get(i)));

            year++;
            coordinatesList.add(point);

        }

        coordinates = new DataPoint[coordinatesList.size()];
        coordinates = coordinatesList.toArray(coordinates);

        series = new LineGraphSeries<>( coordinates);

        //setting title
       lblGraphTitle.setText("Carbon Emission for " + selectedCountry.toUpperCase());


        graph.getGridLabelRenderer().setHorizontalAxisTitle(getString(R.string.carbonEmissionXAxisLabel));
        graph.getGridLabelRenderer().setVerticalAxisTitle(getString(R.string.carbonEmissionYAxisLabel));

        graph.addSeries(series);

        //setting graph display colors
        graph.getGridLabelRenderer().setGridColor(Color.WHITE);
        graph.getGridLabelRenderer().setHorizontalAxisTitleColor(Color.WHITE);
        graph.getGridLabelRenderer().setVerticalAxisTitleColor(Color.WHITE);
        graph.getGridLabelRenderer().setHorizontalLabelsColor(Color.WHITE);
        graph.getGridLabelRenderer().setVerticalLabelsColor(Color.WHITE);
        graph.getGridLabelRenderer().setLabelsSpace(5);

        //Setting bounds on x-axis
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX(2000);
        graph.getViewport().setMaxX(2020);

        series.setAnimated(true);

        //Show a snackbar that allows the Users to choose another country, Add support and design as dependencies and use coordinator layout as the top-most container layout
        snackbar = Snackbar.make(findViewById(R.id.coordinatorLayoutCarbonEmissionDisplay),R.string.snackBarSelectAnotherCountry,Snackbar.LENGTH_SHORT); //parameters are parentView, string content of snackbar, durataion
        snackbar.setActionTextColor(Color.CYAN);
        snackbar.setDuration(10000);
        snackbar.setAction(R.string.snackBarActionSelectAnotherCountry, new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              Intent restartIntent = new Intent(getApplicationContext(), CarbonEmission.class);
              startActivity(restartIntent);
            }
        });

        //we want the snackbar to be shown after 3 seconds
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                snackbar.show(); //snack bar will be displayed
            }
        }, 3000);


    }

    /**
     * Options Menu with StartScreen button
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.climate_home_button_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.opMenuItemHome:
                Toast.makeText(this, "StartScreen menu item clicked", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, StartScreen.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }







}
