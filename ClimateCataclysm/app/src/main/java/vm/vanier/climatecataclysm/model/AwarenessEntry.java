package vm.vanier.climatecataclysm.model;

/**
 * AwarenessEntry allows the User to give their response to a given climate aspect
 *
 * @author Mauran P.
 * @version Version 2
 *
 */
public class AwarenessEntry {

    private int entryId;
    private String entryAspect;
    private String entryResponse;
    private boolean clickedStatus=false;


    public AwarenessEntry(){


    }

    public AwarenessEntry(String entryAspect, String entryResponse){

        this.entryAspect = entryAspect;
        this.entryResponse = entryResponse;

    }

    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }

    public String getEntryAspect() {
        return entryAspect;
    }

    public void setEntryAspect(String entryAspect) {
        this.entryAspect = entryAspect;
    }

    public String getEntryResponse() {
        return entryResponse;
    }

    public void setEntryResponse(String entryResponse) {
        this.entryResponse = entryResponse;
    }

    public boolean isClickedStatus() {
        return clickedStatus;
    }

    public void setClickedStatus(boolean clickedStatus) {
        this.clickedStatus = clickedStatus;
    }

    public String toString() {

        return this.entryId + " " + this.entryAspect + ":\n" + this.entryResponse;
    }












}
