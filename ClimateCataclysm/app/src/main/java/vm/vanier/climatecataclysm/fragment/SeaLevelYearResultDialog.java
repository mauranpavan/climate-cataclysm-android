package vm.vanier.climatecataclysm.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import vm.vanier.climatecataclysm.R;


/**
 * Displays the rate of change in sea level in terms of percentage
 *
 * @author Mauran P.
 * @version Version 1
 */
public class SeaLevelYearResultDialog extends DialogFragment {

    private TextView lblPercentageChange;
    private TextView lblYearResultDesc;
    private Button btnClose;
    private String percentageChangeStr;
    private String yearA;
    private String yearB;

    public SeaLevelYearResultDialog() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sea_level_year_result_dialog, container, false);


        //initialize ui components
        lblPercentageChange = (TextView) view.findViewById(R.id.lblPercentageChange);
        lblYearResultDesc = (TextView) view.findViewById(R.id.lblYearResultDesc);
        btnClose = (Button) view.findViewById(R.id.btnSeaYearResultClose);

        //retrieve percentage string value from Bundle by getArguments (in the activity, we setArguments for the fragment)
        percentageChangeStr = getArguments().getString("percentageChangeStr");

        //set it to the TextView
        lblPercentageChange.setText(percentageChangeStr + "%");

        //retrieve the two year values
        yearA = getArguments().getString("yearA");
        yearB = getArguments().getString("yearB");

        lblYearResultDesc.setText("From " + yearA +" To " + yearB);


        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return view;
    }

}
