package vm.vanier.climatecataclysm.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeMap;

import vm.vanier.climatecataclysm.utils.ParseCSV;
import vm.vanier.climatecataclysm.R;

/**
 * Displays a graphic representation of the sea level rise from 1993 to 2015
 *
 * @author Mauran P
 * @version Version 1
 */
public class SeaLevelGraphFragment extends Fragment {

    private static String TAG = "ClimateCataclysm";
    private LineGraphSeries<DataPoint> series;
    private ArrayList<DataPoint> coordinatesList;
    private DataPoint[] coordinates;
    private TreeMap<String,String> map;


    private Button btnEnterYear;


    public SeaLevelGraphFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_sea_level_graph, container, false);

        //ui components
        btnEnterYear = (Button) view.findViewById(R.id.btnSeaLevel1GraphDialog);

        //Creating a graph
        GraphView graph = (GraphView) view.findViewById(R.id.seaLevelGraph);

        coordinatesList =new ArrayList<>();


        //Get the data points from the database of sea level rise
        InputStream inputStream = getResources().openRawResource(R.raw.global_sea_level_rise);

        ParseCSV parseCsv = new ParseCSV();

        try {
            //A hashmap to store the values we retrieve from csv file, but because we need to have the key values in asc order to plot the graph, we will use TreeMap
            map = new TreeMap<>(parseCsv.parse(inputStream));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Iterator itr = map.keySet().iterator();

        while(itr.hasNext()){
            String key = itr.next().toString();
            DataPoint point = new DataPoint(Double.parseDouble(key), Double.parseDouble(map.get(key)));
            coordinatesList.add(point);
        }

        //We have to order our coordinates in ascending order to plot the graph
        for (DataPoint value:coordinatesList) {
            Log.d(TAG, "onCreateView: " + coordinatesList);

        }

        coordinates = new DataPoint[coordinatesList.size()];
        coordinates = coordinatesList.toArray(coordinates);

        series = new LineGraphSeries<>( coordinates);

        //setting title
        series.setTitle("Rise in Global Sea Level (in mm) since 1993");

        graph.addSeries(series);

        //Setting bounds on x-axis
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX(1993);
        graph.getViewport().setMaxX(2020);

        //Setting bounds on y-axis
        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMinY(-10);
        graph.getViewport().setMaxY(90);



        //Enabling scrolling and zooming: Not working
       //graph.getViewport().setScalable(true); //this is for horizontal zooming and scrolling
       //graph.getViewport().setScalableY(true); //this is for vertical zooming and scrolling

        series.setAnimated(true);


        //This will open a DialogFragment in order to get User Input
        btnEnterYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d(TAG, "onClick: Enter Year accessed");
                //we are telling the activity to open the YearInputDialog when the "Enter Year" button in the SeaLevelGraphFragment is clicked
                ISeaLevelDialogListener activity = (ISeaLevelDialogListener) getActivity();
                activity.startYearInputDialog();

            }
        });

        return view;
    }

}
