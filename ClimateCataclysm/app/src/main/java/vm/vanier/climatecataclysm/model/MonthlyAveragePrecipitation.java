package vm.vanier.climatecataclysm.model;

import java.util.ArrayList;

/**
 * Store relevant attributes of monthly avg precipitation
 *
 * @author Mauran P.
 * @version Version 2
 */
public class MonthlyAveragePrecipitation {

    private String selectedCountry;
    private String fromYear;
    private String toYear;
    private ArrayList<Double> monthlyAvgList;

    public MonthlyAveragePrecipitation(){

        this.monthlyAvgList = new ArrayList<>();
    }

    public MonthlyAveragePrecipitation(String selectedCountry, String fromYear, String toYear, ArrayList<Double> monthlyAvgList){

        this.selectedCountry = selectedCountry;
        this.fromYear = fromYear;
        this.toYear = toYear;
        this.monthlyAvgList = new ArrayList<>(monthlyAvgList);
    }

    public String getSelectedCountry() {
        return selectedCountry;
    }

    public void setSelectedCountry(String selectedCountry) {
        this.selectedCountry = selectedCountry;
    }


    public ArrayList<Double> getMonthlyAvgList() {
        return monthlyAvgList;
    }

    public void setMonthlyAvgList(ArrayList<Double> monthlyAvgList) {
        this.monthlyAvgList = monthlyAvgList;
    }

    public String getFromYear() {
        return fromYear;
    }

    public void setFromYear(String fromYear) {
        this.fromYear = fromYear;
    }

    public String getToYear() {
        return toYear;
    }

    public void setToYear(String toYear) {
        this.toYear = toYear;
    }


    @Override
    public String toString() {

        return String.format("From %s to to %s, monthly avg values are ",fromYear, toYear) + monthlyAvgList;

    }
}
