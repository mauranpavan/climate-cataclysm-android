package vm.vanier.climatecataclysm.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import vm.vanier.climatecataclysm.R;
import vm.vanier.climatecataclysm.model.AwarenessEntry;

/**
 * Adapter used for SpreadAwareness
 *
 * @author Mauran P.
 * @version Version 2
 */
public class SpreadAwarenessAdapter extends RecyclerView.Adapter<SpreadAwarenessAdapter.ViewHolder> {

    private static final String TAG = "SpreadAwarenessAdapter";
    private ArrayList<AwarenessEntry> awarenessList;
    private int position;

    // ViewHolder holds the references to the UI components for each row
    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageViewAwarenessItem;
        public TextView lblAwarenessItemTitle;
        public TextView lblAwarenessItemContent;



        public ViewHolder(final View itemView) {
            super(itemView);

            imageViewAwarenessItem = itemView.findViewById(R.id.imageViewAwarenessItem);
            lblAwarenessItemTitle = itemView.findViewById(R.id.lblAwarenessItemTitle);
            lblAwarenessItemContent = itemView.findViewById(R.id.lblAwarenessItemContent);

             position = getAdapterPosition();

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.d(TAG, "onClick: " + lblAwarenessItemContent.getText().toString());
                    //Toast.makeText(v.getContext(),lblAwarenessItemContent.getText().toString(),Toast.LENGTH_SHORT).show();
                    Toast.makeText(v.getContext(),""+ position,Toast.LENGTH_SHORT).show();


                    //pass the view with interface to the activity
                }
            });

        }
    }

    //Constructor
    public SpreadAwarenessAdapter(ArrayList<AwarenessEntry> entries){
        this.awarenessList = entries;
    }


    @NonNull
    @Override
    public SpreadAwarenessAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);


        View awarenessView = inflater.inflate(R.layout.item_awareness, viewGroup, false);

        ViewHolder viewHolder = new ViewHolder(awarenessView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SpreadAwarenessAdapter.ViewHolder viewHolder, int i) {

        //Retrieve data
        AwarenessEntry  awarenessEntry = awarenessList.get(i);

       //Initialize ui components according to viewHolder
        ImageView imageViewIcon = viewHolder.imageViewAwarenessItem;
        TextView lblTitle = viewHolder.lblAwarenessItemTitle;
        TextView lblContent = viewHolder.lblAwarenessItemContent;

        //Set data to UI
        lblTitle.setText(awarenessEntry.getEntryAspect());
        lblContent.setText(awarenessEntry.getEntryResponse());



        switch (awarenessEntry.getEntryAspect().toLowerCase()) {
            case "temperature":
                imageViewIcon.setImageResource(R.drawable.temperature);
                break;
            case "precipitation":
                imageViewIcon.setImageResource(R.drawable.ic_precipitation);
                break;
            case "carbon emission":
                imageViewIcon.setImageResource(R.drawable.ic_carbon_emission);
                break;
            case "ozone layer":
                imageViewIcon.setImageResource(R.drawable.ic_ozone_layer);
                break;
            case "sea level rise":
                imageViewIcon.setImageResource(R.drawable.ic_sea_level_rise);
                break;
            default:
                    imageViewIcon.setImageResource(R.drawable.ic_earth);

        }

    }

    @Override
    public int getItemCount() {
        return awarenessList.size();
    }
}
