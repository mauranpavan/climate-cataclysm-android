package vm.vanier.climatecataclysm.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.List;

import vm.vanier.climatecataclysm.R;
import vm.vanier.climatecataclysm.adapters.ClimateAdapter;
import vm.vanier.climatecataclysm.controller.ClimateController;
import vm.vanier.climatecataclysm.model.TemperatureItem;

public class TemperatureUser extends Activity {
    private ListView listView;
    private Button btnAdd;
    private Button btnDelete;
    private Button btnSearch;
    private Button btnUpdate;
    private List<TemperatureItem> temperatureItemList;
    private ClimateController climateController;
    private String db_name = "temperatureList";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature_user);

        listView = (ListView) findViewById(R.id.listTemperatures);
        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnDelete = (Button) findViewById(R.id.btnDelete);
        btnSearch = (Button) findViewById(R.id.btnFind);
        btnUpdate = (Button) findViewById(R.id.btnUpdate);

        climateController = new ClimateController();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                updateTemperateItem(position);
            }
        });

        //climateController.createTableTemperature(db_name);

    }

    @Override
    protected void onResume() {
        super.onResume();
        //updateListView();
    }

    //should be used on resume, updated list.
    private void updateListView() {
        this.temperatureItemList = this.climateController.getAllTemperatures();
        ClimateAdapter adapter = new ClimateAdapter(this, this, temperatureItemList);
        this.listView.setAdapter(adapter);
    }

    public void onClickAdd(View view) {
                addTemperatureItem();
    }

    private void addTemperatureItem() {
        Intent intent = new Intent(this, TemperatureInput.class);
        startActivity(intent);
    }

    private void updateTemperateItem(int index) {
        TemperatureItem temperatureItem = temperatureItemList.get(index);
        Intent intent = new Intent(this, TemperatureUser.class);
        intent.putExtra("TemperatureItem", temperatureItem);
        startActivity(intent);
    }


    public void onClickFind(View view) {
        Intent intent = new Intent(this, TemperatureFind.class);
        startActivity(intent);
    }

    public void onClickUpdate(View view) {
        Intent intent = new Intent(this, TemperatureUpdate.class);
        startActivity(intent);
    }

    public void onClickDelete(View view) {
        Intent intent = new Intent(this, TemperatureDelete.class);
        startActivity(intent);
    }

}
