package vm.vanier.climatecataclysm.controller;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Controller that makes api calls by establishing an HttpURLConnection and returns the api response as a string
 *
 * @author Mauran P.
 * @version Version 3
 */
public class PrecipitationAPIController {

    private static final String TAG = "PrecipitationAPIController";
    private String response = "";
    private int responseCode;

    public PrecipitationAPIController() {
    }

    public String callAPI(String urlStr) {


        try {
            URL url = new URL(urlStr);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.connect();

            responseCode = conn.getResponseCode();

            if(responseCode == HttpURLConnection.HTTP_OK){ //http_ok is 200 code. http_accepted means that the request has been received but it may or maybe be processed

                InputStream in = conn.getInputStream();


                StringBuilder stringBuilder = new StringBuilder();
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                String line;
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line);
                }
                response = stringBuilder.toString();


            }
            else
                Log.e(TAG, "callAPI: Connection was not successful" );

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
         return  response;
    }
}

