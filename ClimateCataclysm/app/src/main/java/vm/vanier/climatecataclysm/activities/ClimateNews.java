package vm.vanier.climatecataclysm.activities;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;


import vm.vanier.climatecataclysm.adapters.ClimateNewsAdapter;
import vm.vanier.climatecataclysm.R;
import vm.vanier.climatecataclysm.model.NewsArticle;

/**
 * Displays the available climate news the User could read
 *
 * @author Mauran P
 * @version Version 1
 *
 * References:
 * -https://guides.codepath.com/android/using-the-recyclerview
 * - API call example: https://newsapi.org/v2/everything?q=climatechange&language=en&apiKey=ea4441f900fd4eadb6e04216fc2ec4d8
 */

public class ClimateNews extends AppCompatActivity {

    private ArrayList<NewsArticle> articles;
    private Intent intent;
    private static final String CHANNEL_ID = "MainChannel";
    private static final int NOTIFICATION_ID=42;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_climate_news);


        //Locate the recycle view in the activity layout
        RecyclerView rvNewsArticle = (RecyclerView) findViewById(R.id.rvNewsArticle);


        //Create and send notification
        sendSpreadAwarenessNotification();




        //using arraylist as data source
        articles = new ArrayList<>();

        for (int i = 0; i <= 20; i++) {
            NewsArticle article = new NewsArticle("news article " + i);
               articles.add(article);
            }





        registerForContextMenu(rvNewsArticle);

        ClimateNewsAdapter adapter = new ClimateNewsAdapter(articles);

        rvNewsArticle.setAdapter(adapter);

        rvNewsArticle.setLayoutManager(new LinearLayoutManager(this));

    }

    private void sendSpreadAwarenessNotification() {

        //Instantiating notification manager
        NotificationManager notificationManager  = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        //Creating a notification object
        NotificationCompat.Builder myNotification = new NotificationCompat.Builder(getApplicationContext(),CHANNEL_ID);

        //setting a style for our notification
        NotificationCompat.BigTextStyle style = new NotificationCompat.BigTextStyle(); //this will allows us to display larger texts

        //Instantiating intent for notification
        Intent intent = new Intent(this, SpreadAwareness.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        myNotification.setContentTitle("Spread Awareness");
        myNotification.setContentText("Had any new thoughts on climate change? Write them down before you forget.");
        myNotification.setSmallIcon(R.drawable.ic_earth);
        myNotification.setColor(Color.GREEN); //color of the icon in the notification
        myNotification.setColorized(true);
        myNotification.setStyle(style); //sets the style
        myNotification.addAction(0,"Write",pendingIntent);
        myNotification.setAutoCancel(true); //this will remove the notification when the user clicks on it


        //Notifying the notification manager
        notificationManager.notify(NOTIFICATION_ID,myNotification.build());
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        getMenuInflater().inflate(R.menu.climate_news_context_menu, menu);


    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {


        switch(item.getItemId()){
            case R.id.menuItemEdit:
                Toast.makeText(this, item.getTitle(),Toast.LENGTH_SHORT).show();
                return true;
            case R.id.menuItemDelete:
                Toast.makeText(this, item.getTitle(),Toast.LENGTH_SHORT).show();
                return true;

            default:
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
                return super.onContextItemSelected(item);
        }

    }

    /**
     * Options Menu with StartScreen button
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.climate_home_button_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.opMenuItemHome:
                Toast.makeText(this, "StartScreen menu item clicked", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, StartScreen.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



}
