package vm.vanier.climatecataclysm.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import vm.vanier.climatecataclysm.R;

/**
 * Allows the User to select a country to which carbon emission information would be shown
 *
 * @author Mauran P.
 * @version Version 3
 *
 */
public class CarbonEmissionCountries extends AppCompatActivity {

    private Button btnAlgeria;
    private Button btnAustralia;
    private Button btnBrazil;
    private Button btnCanada;
    private Button btnChina;
    private Button btnFrance;
    private Button btnIndia;
    private Button btnIran;
    private Button btnJapan;
    private Button btnMexico;
    private Button btnUnitedKingdom;
    private Button btnUnitedStatesOfAmerica;
    private Intent countryIntent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carbon_emission_countries);

        countryIntent = new Intent();

        //Initialize ui components
        btnAlgeria = findViewById(R.id.btnAlgeria);
        btnAustralia = findViewById(R.id.btnAustralia);
        btnBrazil = findViewById(R.id.btnBrazil);
        btnCanada = findViewById(R.id.btnCanada);
        btnChina = findViewById(R.id.btnChina);
        btnFrance = findViewById(R.id.btnFrance);
        btnIndia = findViewById(R.id.btnIndia);
        btnIran = findViewById(R.id.btnIran);
        btnJapan = findViewById(R.id.btnJapan);
        btnMexico = findViewById(R.id.btnMexico);
        btnUnitedKingdom = findViewById(R.id.btnUnitedKingdom);
        btnUnitedStatesOfAmerica = findViewById(R.id.btnUnitedStatesOfAmerica);


        btnAlgeria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countrySelected(btnAlgeria.getText().toString());
            }
        });
        btnAustralia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countrySelected(btnAustralia.getText().toString());
            }
        });
        btnBrazil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countrySelected(btnBrazil.getText().toString());
            }
        });
        btnCanada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countrySelected(btnCanada.getText().toString());
            }
        });
        btnChina.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countrySelected(btnChina.getText().toString());
            }
        });
        btnFrance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countrySelected(btnFrance.getText().toString());
            }
        });
        btnIndia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countrySelected(btnIndia.getText().toString());
            }
        });
        btnIran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countrySelected(btnIran.getText().toString());
            }
        });
        btnJapan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countrySelected(btnJapan.getText().toString());
            }
        });
        btnMexico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countrySelected(btnMexico.getText().toString());
            }
        });
        btnUnitedKingdom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countrySelected(btnUnitedKingdom.getText().toString());
            }
        });
        btnUnitedStatesOfAmerica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countrySelected(btnUnitedStatesOfAmerica.getText().toString());
            }
        });
    }

    public void countrySelected(String countryName){

        countryIntent.putExtra("country",countryName.toLowerCase());
        setResult(RESULT_OK,countryIntent);
        this.finish();


    }
}
