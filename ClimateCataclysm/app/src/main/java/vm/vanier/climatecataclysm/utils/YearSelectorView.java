package vm.vanier.climatecataclysm.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import java.util.jar.Attributes;

public class YearSelectorView extends View {

    private float tlWidth = 20;
    private float tlHeight = 20;
    private float tlStartX = 20;
    private float tlStartY = 20;
    private Paint tlTextPaint;
    private Paint tlDialPaint;
    private float textStartX = tlStartX + 10;
    private float textStartY = tlStartY + 10;
    private int startYear = 1900;
    private int finishYear = 2000;
    private int yearLeaps = 10;
    private int counter = 0;



    public YearSelectorView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public float getTlWidth() {
        return tlWidth;
    }

    public float getTlHeight() {
        return tlHeight;
    }

    public float getTlStartX() {
        return tlStartX;
    }

    public float getTlStartY() {
        return tlStartY;
    }

    public Paint getTlTextPaint() {
        return tlTextPaint;
    }

    public Paint getTlDialPaint() {
        return tlDialPaint;
    }

    public float getTextStartX() {
        return textStartX;
    }

    public float getTextStartY() {
        return textStartY;
    }

    public int getStartYear() {
        return startYear;
    }

    public int getFinishYear() {
        return finishYear;
    }

    public int getYearLeaps() {
        return yearLeaps;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawRectangle(canvas, getTlStartX(), getTlStartY(), getTlWidth(), getTlHeight());

        for(int i = getStartYear(), gap = 20; i < getFinishYear(); i=+getYearLeaps()){
            canvas.drawText(Integer.toString(i), getTextStartX() + gap, getTextStartY(), getTlTextPaint());
        }
    }

    private void init() {
        tlTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        tlTextPaint.setColor(Color.BLACK);
        tlTextPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        tlTextPaint.setTextAlign(Paint.Align.CENTER);
        tlTextPaint.setTextSize(40f);
        tlDialPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        tlDialPaint.setColor(Color.GRAY);
    }

    private void drawRectangle(Canvas canvas, float startX, float startY, float width, float height){
        canvas.drawLine(startX, startY, width, startY, tlTextPaint);
        canvas.drawLine(startX + width, startY, startX + width, startY - height, tlTextPaint);
        canvas.drawLine(startX + width, startY - height, startX, startY - height, tlTextPaint);
        canvas.drawLine(startX, startY - height, startX, startY, tlTextPaint);
    }

    private OnClickListener yearListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            int numParts = getFinishYear() - getStartYear() / getYearLeaps();
            int c = getCounter();
            tlDialPaint.setColor(Color.GREEN);
            setCounter(c++);
            invalidate();
        }
    };
}

