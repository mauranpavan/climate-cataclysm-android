package vm.vanier.climatecataclysm.fragment;

import android.os.Bundle;

/**
 * Interface associated with SeaLevelRise Activity
 *
 * @author Mauran P.
 * @version Version 1
 */
public interface ISeaLevelDialogListener {

        void startYearInputDialog(); //open a dialog to gather user input
        void onFinishYearInputDialog(int yearA, int yearB);
        void startYearResultDialog(Bundle bundle); //open a dialog to display the rate of change is sea level

}
