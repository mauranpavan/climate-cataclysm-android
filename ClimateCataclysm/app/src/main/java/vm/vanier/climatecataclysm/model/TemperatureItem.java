package vm.vanier.climatecataclysm.model;

import android.os.Parcel;
import android.os.Parcelable;

public class TemperatureItem implements Parcelable {
    private String country;
    private String date;
    private String temperature;

    public TemperatureItem() {
    }

    public TemperatureItem(Parcel in) {
        country = in.readString();
        date = in.readString();
        temperature = in.readString();
    }

    public static final Creator<TemperatureItem> CREATOR = new Creator<TemperatureItem>() {
        @Override
        public TemperatureItem createFromParcel(Parcel in) {
            return new TemperatureItem(in);
        }

        @Override
        public TemperatureItem[] newArray(int size) {
            return new TemperatureItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(country);
        dest.writeString(date);
        dest.writeString(temperature);
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }


    @Override
    public String toString() {
        return "country: " + country + " date: " + date + " temperature: " + temperature;
    }

}
