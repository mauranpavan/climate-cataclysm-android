package vm.vanier.climatecataclysm.activities;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.Calendar;

import vm.vanier.climatecataclysm.R;
import vm.vanier.climatecataclysm.activities.CarbonEmission;
import vm.vanier.climatecataclysm.activities.SpreadAwareness;

/**
 * Class to handle notifications
 *
 * @author Mauran P.
 * @version Version 3
 *
 * References:
 *  - https://developer.android.com/training/scheduling/alarms#java
 */
public class Notifications extends AppCompatActivity{

    private static final String CHANNEL_ID = "MainChannel";
    private static final int NOTIFICATION_ID=42;
    private  AlarmManager alarmManager;
    private Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        //calendar.set(Calendar.HOUR_OF_DAY, 18); //for 6pm

        Intent intent = new Intent(this, CarbonEmission.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0); //Retrieve a PendingIntent that will perform a broadcast

        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),1000 * 60 * 1, alarmIntent); //interval set at every min

        sendSpreadAwarenessNotification();

    }


    private void sendSpreadAwarenessNotification() {

        //Instantiating notification manager
        NotificationManager notificationManager  = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        //Creating a notification object
        NotificationCompat.Builder myNotification = new NotificationCompat.Builder(getApplicationContext(),CHANNEL_ID);

        //setting a style for our notification
        NotificationCompat.BigTextStyle style = new NotificationCompat.BigTextStyle(); //this will allows us to display larger texts

        //Instantiating intent for notification
        Intent intent = new Intent(this, SpreadAwareness.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        myNotification.setContentTitle("Spread Awareness");
        myNotification.setContentText("Had any new thoughts on climate change? Write them down before you forget.");
        myNotification.setSmallIcon(R.drawable.ic_earth);
        myNotification.setColor(Color.GREEN); //color of the icon in the notification
        myNotification.setColorized(true);
        myNotification.setStyle(style); //sets the style
        myNotification.addAction(0,"Write",pendingIntent);

        myNotification.setAutoCancel(true); //this will remove the notification when the user clicks on it


        //Notifying the notification manager
        notificationManager.notify(NOTIFICATION_ID,myNotification.build());

    }

    /**
     * Options Menu with StartScreen button
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.climate_home_button_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.opMenuItemHome:
                Toast.makeText(this, "StartScreen menu item clicked", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, StartScreen.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
