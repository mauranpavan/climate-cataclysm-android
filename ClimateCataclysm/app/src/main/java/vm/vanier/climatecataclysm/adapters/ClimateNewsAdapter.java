package vm.vanier.climatecataclysm.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import vm.vanier.climatecataclysm.R;
import vm.vanier.climatecataclysm.model.NewsArticle;

/**
 * Adapter used for ClimateNews
 *
 * @author Mauran P
 * @version Version 1
 */
public class ClimateNewsAdapter extends RecyclerView.Adapter<ClimateNewsAdapter.ViewHolder>{

  public class ViewHolder extends RecyclerView.ViewHolder {

      public TextView lblNewsTitle;
      public Button btnOpenArticle;



      public ViewHolder(View itemView) {
            super(itemView);

          lblNewsTitle = (TextView) itemView.findViewById(R.id.lblClimateNewsItem);
          btnOpenArticle = (Button) itemView.findViewById(R.id.btnClimateNewsRead);
          itemView.setLongClickable(true);

      }
  }

  private ArrayList<NewsArticle> articleList;

  //Constructor
   public ClimateNewsAdapter(ArrayList<NewsArticle> articles){

       this.articleList = articles;
   }


    @Override
    public ClimateNewsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);


        View contactView = inflater.inflate(R.layout.item_news, viewGroup, false);


        ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;
    }

    @Override
    /**
     * To populate the data into the item
     */
    public void onBindViewHolder(@NonNull ClimateNewsAdapter.ViewHolder viewHolder, int i) {

        //Get the data
        NewsArticle newsArticle = articleList.get(i);

        TextView txtView = viewHolder.lblNewsTitle;
        txtView.setText(newsArticle.getTitle());



    }

    @Override
    /**
     * Returns the total count of NewsArticle items in the list
     */
    public int getItemCount() {
        return articleList.size();
    }




}
