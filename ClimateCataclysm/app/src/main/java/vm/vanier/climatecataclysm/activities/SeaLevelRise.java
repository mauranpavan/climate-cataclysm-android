package vm.vanier.climatecataclysm.activities;


import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import vm.vanier.climatecataclysm.R;
import vm.vanier.climatecataclysm.fragment.ISeaLevelDialogListener;
import vm.vanier.climatecataclysm.fragment.SeaLevelGalleryFragment;
import vm.vanier.climatecataclysm.fragment.SeaLevelGraphFragment;
import vm.vanier.climatecataclysm.fragment.SeaLevelYearResultDialog;
import vm.vanier.climatecataclysm.fragment.YearInputDialog;

/**
 * Provides information to the User about the Sea Level Rise through an image gallery and through graphical display of global sea level rise data
 *
 * @author Mauran P
 * @version Version 1
 */
public class SeaLevelRise extends AppCompatActivity implements ISeaLevelDialogListener {

    private static String TAG = "ClimateCataclysm";
    private Button btnImages;
    private Button btnGraph;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sea_level_rise);


        //we initialize ui components
        btnImages = (Button) findViewById(R.id.btnSeaLevel1Images);
        btnGraph = (Button) findViewById(R.id.btnSeaLevel1Graph);

        btnGraph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // get fragment manager
                FragmentManager fm = getSupportFragmentManager();

                FragmentTransaction ftReplace = fm.beginTransaction();

               // get the fragment container which is seaDisplayFrag and replace the existing fragment with the new SeaLevelGraphFragment
                ftReplace.replace(R.id.seaDisplayFrag, new SeaLevelGraphFragment());
                ftReplace.commit();

                Log.d(TAG, "onClick: Graph accessed.");
            }
        });

        btnImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // get fragment manager
                FragmentManager fm = getSupportFragmentManager();

                FragmentTransaction ftReplace = fm.beginTransaction();

                // get the fragment container which is seaDisplayFrag and replace the existing fragment with the new SeaLevelGraphFragment
                ftReplace.replace(R.id.seaDisplayFrag, new SeaLevelGalleryFragment());
                ftReplace.commit();
                Log.d(TAG, "onClick: Gallery accessed");

            }
        });

    }

    @Override
    public void startYearInputDialog() {

        //we open the dialog for year input
        FragmentManager fm = getSupportFragmentManager();
        YearInputDialog inputDialog = new YearInputDialog();
        inputDialog.show(fm, TAG);

    }

    @Override
    public void onFinishYearInputDialog(int yrA, int yrB) {
        int yearA=yrA;
        int yearB=yrB;
        double percentageChange=0;
        String percentageChangeStr="";

        Toast.makeText(this, "" + yrA + " " + yrB, Toast.LENGTH_SHORT).show();

        if(yrA > yrB){
            yearA = yrB; //lowest value would be yearA
            yearB = yrA;
        }


        //Calcute rate of change
        double seaLevelA;
        double seaLevelB;
        double rateOfChange = 0;

        seaLevelA = 3.2158 * yearA - 6408.6; //lowest sea level value would be in seaLevelA
        seaLevelB = 3.2158 * yearB - 6408.6;

        percentageChange = (seaLevelB - seaLevelA)/seaLevelA  * 100;

        percentageChangeStr = String.format("%,.2f", percentageChange);

        Bundle bundle = new Bundle();
        bundle.putString("percentageChangeStr",percentageChangeStr);
        bundle.putString("yearA", ""+ yearA);
        bundle.putString("yearB", ""+ yearB);

        //open the dialog
       startYearResultDialog(bundle);


    }

    @Override
    public void startYearResultDialog(Bundle bundle) {

        //we open the dialog to display the rate of change in sea levels
        FragmentManager fm = getSupportFragmentManager();
        SeaLevelYearResultDialog displayDialog = new SeaLevelYearResultDialog();
        displayDialog.setArguments(bundle);
        displayDialog.show(fm, TAG);

    }

    /**
     * Options Menu with home button
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.climate_home_button_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.opMenuItemHome:
                Toast.makeText(this, "Home menu item clicked", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, StartScreen.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }






}
