package vm.vanier.climatecataclysm.utils;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

/**
 * Allows to parse a given csv file
 *
 * @author Mauran P.
 * @version Version 1
 *
 */
public class ParseCSV {

    private static String TAG = "ClimateCataclysm";




    public ParseCSV(){

    }


    public HashMap<String,String> parse(InputStream inputStream) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String[] nextLine={""};
        String eachLine="";
        HashMap<String,String> map = new HashMap<>();
        try {

            while ((eachLine = reader.readLine())!= null) {
                nextLine= eachLine.split(",");

               map.put(nextLine[0],nextLine[1]);

                Log.d(TAG, "parse: " + nextLine[0] + "|" + nextLine[1] + "|");

            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return map;

    }















}
