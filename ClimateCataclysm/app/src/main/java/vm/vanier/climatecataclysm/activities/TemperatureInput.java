package vm.vanier.climatecataclysm.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import vm.vanier.climatecataclysm.R;
import vm.vanier.climatecataclysm.controller.ClimateController;
import vm.vanier.climatecataclysm.model.TemperatureItem;

public class TemperatureInput extends Activity {
    private EditText country;
    private EditText date;
    private EditText temperature;
    private Button btnAccept;
    private Button btnCancel;
    private TemperatureItem currentTemperatureItem;
    private ClimateController climateController;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature_input);


        country = (EditText) findViewById(R.id.txtLocation);
        date = (EditText) findViewById(R.id.txtDate);
        temperature = (EditText) findViewById(R.id.txtTemperature);
        btnAccept = (Button) findViewById(R.id.btnSubmitTemperatureInputDialog);
        btnCancel = (Button) findViewById(R.id.btnCancelTemperatureInputDialog);
        climateController = new ClimateController(this);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if(bundle != null){
            currentTemperatureItem = (TemperatureItem) bundle.get("TemperatureItem");
            if(currentTemperatureItem != null){
                country.setText(currentTemperatureItem.getCountry());
                date.setText(currentTemperatureItem.getDate());
                temperature.setText(currentTemperatureItem.getTemperature());
            }
        }
        
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( currentTemperatureItem != null){  // 1 == 1
                    addTemperatureItem();
                }   else{
                    updateTemperatureItem();
                }
            }
        });
        
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeTemperatureItem();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void removeTemperatureItem() {
        climateController.removeTemperature(currentTemperatureItem);
        this.finish();
    }

    private void updateTemperatureItem() {
        TemperatureItem temperatureItem = new TemperatureItem();
        temperatureItem.setTemperature(temperature.getText().toString());
        temperatureItem.setCountry(country.getText().toString());
        temperatureItem.setDate(date.getText().toString());
        this.climateController.updateTemperature(currentTemperatureItem, temperatureItem);
        this.finish();
    }

    private void addTemperatureItem() {

        TemperatureItem temperatureItem = new TemperatureItem();
        temperatureItem.setTemperature(temperature.getText().toString());
        temperatureItem.setCountry(country.getText().toString());
        temperatureItem.setDate(date.getText().toString());
        Toast.makeText(this, temperatureItem.getCountry(), Toast.LENGTH_LONG);
        this.climateController.addTemperature(temperatureItem);

    }


}
