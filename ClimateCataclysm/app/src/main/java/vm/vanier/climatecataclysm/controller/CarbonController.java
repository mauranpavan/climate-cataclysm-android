package vm.vanier.climatecataclysm.controller;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/***
 *  Controller that serves as reader between a view and the carbon table
 *
 * @author Mauran P.
 * @version Version 2
 *
 */
public class CarbonController {

    /**
     * TAG name for logcat testing
     */
    private static final String TAG = "CarbonController";
    /**
     * Stores name of ClimateChangeDBAccessController
     */
    private ClimateChangeDBAccessController dbAccessHelper;
    /**
     * SQLiteDatabase variable
     */
    private SQLiteDatabase database;
    /**
     * Database table name
     */
    private String DB_TABLE_NAME = "carbon";
    /**
     * Column name that stores country name
     */
    private static final String COLUMN_COUNTRY="country_name";
    /**
     * Column name that stores the carbon emission for the year 2000
     */
    private static final String COLUMN_Y2000="year_2000";
    /**
     * Column name that stores the carbon emission for the year 2001
     */
    private static final String COLUMN_Y2001="year_2001";
    /**
     * Column name that stores the carbon emission for the year 2002
     */
    private static final String COLUMN_Y2002="year_2002";
    /**
     * Column name that stores the carbon emission for the year 2003
     */
    private static final String COLUMN_Y2003="year_2003";
    /**
     * Column name that stores the carbon emission for the year 2004
     */
    private static final String COLUMN_Y2004="year_2004";
    /**
     * Column name that stores the carbon emission for the year 2005
     */
    private static final String COLUMN_Y2005="year_2005";
    /**
     * Column name that stores the carbon emission for the year 2006
     */
    private static final String COLUMN_Y2006="year_2006";
    /**
     * Column name that stores the carbon emission for the year 2007
     */
    private static final String COLUMN_Y2007="year_2007";
    /**
     * Column name that stores the carbon emission for the year 2008
     */
    private static final String COLUMN_Y2008="year_2008";
    /**
     * Column name that stores the carbon emission for the year 2009
     */
    private static final String COLUMN_Y2009="year_2009";
    /**
     * Column name that stores the carbon emission for the year 2010
     */
    private static final String COLUMN_Y2010="year_2010";
    /**
     * Column name that stores the carbon emission for the year 2011
     */
    private static final String COLUMN_Y2011="year_2011";
    /**
     * Column name that stores the carbon emission for the year 2012
     */
    private static final String COLUMN_Y2012="year_2012";
    /**
     * Column name that stores the carbon emission for the year 2013
     */
    private static final String COLUMN_Y2013="year_2013";
    /**
     * Column name that stores the carbon emission for the year 2014
     */
    private static final String COLUMN_Y2014="year_2014";
    /**
     * Column name that stores the carbon emission for the year 2015
     */
    private static final String COLUMN_Y2015="year_2015";
    /**
     * Column name that stores the carbon emission for the year 2016
     */
    private static final String COLUMN_Y2016="year_2016";
    /**
     * Column name that stores the carbon emission for the year 2017
     */
    private static final String COLUMN_Y2017="year_2017";

    public CarbonController(Context context){
        this.dbAccessHelper = ClimateChangeDBAccessController.getInstance(context);

    }

    /**
     * Getting carbon emission data for a country requested by the User
     * @param country The country name
     * @return List of Carbon Emission data in the form of double
     */
    public List<String> getEmissionDataByCountry(String country) {

        List<String> carbonEmissionList = new ArrayList<>();


        database = this.dbAccessHelper.openDb();

        try {
            //we want to get all the data from the database, so the cursor must be moved to first
            Cursor cursor = database.rawQuery("SELECT * FROM " + DB_TABLE_NAME,null);
            cursor.moveToFirst();

            while(!cursor.isLast()) {
                cursor.moveToNext();
                if (cursor.getString(cursor.getColumnIndex(COLUMN_COUNTRY)).equalsIgnoreCase(country))
                    break;
            }

            carbonEmissionList.add(Double.toString(cursor.getDouble(cursor.getColumnIndex(COLUMN_Y2000))));
            carbonEmissionList.add(Double.toString(cursor.getDouble(cursor.getColumnIndex(COLUMN_Y2001))));
            carbonEmissionList.add(Double.toString(cursor.getDouble(cursor.getColumnIndex(COLUMN_Y2002))));
            carbonEmissionList.add(Double.toString(cursor.getDouble(cursor.getColumnIndex(COLUMN_Y2003))));
            carbonEmissionList.add(Double.toString(cursor.getDouble(cursor.getColumnIndex(COLUMN_Y2004))));
            carbonEmissionList.add(Double.toString(cursor.getDouble(cursor.getColumnIndex(COLUMN_Y2005))));
            carbonEmissionList.add(Double.toString(cursor.getDouble(cursor.getColumnIndex(COLUMN_Y2006))));
            carbonEmissionList.add(Double.toString(cursor.getDouble(cursor.getColumnIndex(COLUMN_Y2007))));
            carbonEmissionList.add(Double.toString(cursor.getDouble(cursor.getColumnIndex(COLUMN_Y2008))));
            carbonEmissionList.add(Double.toString(cursor.getDouble(cursor.getColumnIndex(COLUMN_Y2009))));
            carbonEmissionList.add(Double.toString(cursor.getDouble(cursor.getColumnIndex(COLUMN_Y2010))));
            carbonEmissionList.add(Double.toString(cursor.getDouble(cursor.getColumnIndex(COLUMN_Y2011))));
            carbonEmissionList.add(Double.toString(cursor.getDouble(cursor.getColumnIndex(COLUMN_Y2012))));
            carbonEmissionList.add(Double.toString(cursor.getDouble(cursor.getColumnIndex(COLUMN_Y2013))));
            carbonEmissionList.add(Double.toString(cursor.getDouble(cursor.getColumnIndex(COLUMN_Y2014))));
            carbonEmissionList.add(Double.toString(cursor.getDouble(cursor.getColumnIndex(COLUMN_Y2015))));
            carbonEmissionList.add(Double.toString(cursor.getDouble(cursor.getColumnIndex(COLUMN_Y2016))));
            carbonEmissionList.add(Double.toString(cursor.getDouble(cursor.getColumnIndex(COLUMN_Y2017))));


          cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "getEmissionDataByCountry:  catch: " + e.getMessage());
        }

        this.dbAccessHelper.closeDb();
        Log.d(TAG, "getEmissionDataByCountry:  db is closed");

        return carbonEmissionList;
    }











}
