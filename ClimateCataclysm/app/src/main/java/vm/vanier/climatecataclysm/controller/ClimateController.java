package vm.vanier.climatecataclysm.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import vm.vanier.climatecataclysm.model.TemperatureItem;

import static android.provider.BaseColumns._ID;

public class ClimateController {

    private static final String TAG = "logger";
    private SQLiteOpenHelper dbHelper;
    private SQLiteDatabase database;
    private String db_table_name = "Temperature Point";
    private String db_name = "temperatureList";
    private static ClimateChangeDBAccessController instance;

    public ClimateController(){

    }

    public ClimateController(Context context){
        this.dbHelper = new ClimateChangeDatabaseOpenHelper(context);
        createTableTemperature(db_name);
    }

    public static ClimateChangeDBAccessController getInstance(Context context){

        if(instance == null){
            instance = new ClimateChangeDBAccessController(context);
        }
        return instance;
    }

   public void createTableTemperature(String db_name){
     database = dbHelper.getWritableDatabase();
     if(isTableCreated(db_name) == false) {
         String SQL_CREATE_TABLE = "CREATE TABLE " + db_name + " (" + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + "country" + " TEXT, " + "date" + " TEXT, " + "temperature" + " TEXT);";
         database.execSQL(SQL_CREATE_TABLE);
     }
     database.close();
   }

   public boolean isTableCreated(String db_name){
        database = dbHelper.getWritableDatabase();
       Cursor cursor = database.rawQuery("SELECT DISTINCT table_name FROM all_tables WHERE table_name = '"+db_name+"'", null);
       if (cursor!=null){
           if(cursor.getCount()>0){
               cursor.close();
               return true;
           }
           cursor.close();
       }
       return false;
   }


    public List<TemperatureItem> getAllTemperatures(){
        List<TemperatureItem> temperatureItemList = new ArrayList<>();
        database = dbHelper.getWritableDatabase();
        try {
            Cursor cursor = database.rawQuery("SELECT * FROM " + db_table_name, null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                TemperatureItem temperatureItem = new TemperatureItem();
                temperatureItem.setCountry(cursor.getString(cursor.getColumnIndex("country")));
                temperatureItem.setDate(cursor.getString(cursor.getColumnIndex("date")));
                temperatureItem.setTemperature(cursor.getString(cursor.getColumnIndex("temperature")));
                temperatureItemList.add(temperatureItem);
                cursor.moveToNext();
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            this.dbHelper.close();
        }

        return temperatureItemList;
    }


    public void updateTemperature(TemperatureItem currentTemperatureItem, TemperatureItem temperatureItem) {
        database = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues(); //stores a set of values initialized below

        values.put("country", temperatureItem.getCountry());
        values.put("temperature", temperatureItem.getTemperature());
        values.put("date", temperatureItem.getDate());

        String query =  " SELECT *  FROM " + db_name + ";";
        Cursor cursor = database.rawQuery(query,null);
        cursor.moveToLast();

        //getCount() returns the number of rows so if the row does not exist, we create it.
        if (cursor.getCount() <= 0)
            database.update(db_name,values,null, new String[]{currentTemperatureItem.getCountry(), currentTemperatureItem.getDate(), currentTemperatureItem.getTemperature()}); //inserts a new row
        database.close();
    }

    public void addTemperature(TemperatureItem temperatureItem) {
        database = dbHelper.getWritableDatabase();
        database.beginTransaction();
        ContentValues values = new ContentValues(); //stores a set of values initialized below


        values.put("country", temperatureItem.getCountry());
        values.put("temperature", temperatureItem.getTemperature());
        values.put("date", temperatureItem.getDate());

        String query =  " SELECT * FROM " + db_name + ";";
        Cursor cursor = database.rawQuery(query,null);
        cursor.moveToLast();

        //getCount() returns the number of rows so if the row does not exist, we create it.
        if (cursor.getCount() <= 0)
            database.insert(db_name, null, values); //inserts a new row
        database.close();
    }

    public void removeTemperature(TemperatureItem currentTemperatureItem) {
        database = dbHelper.getWritableDatabase();
        try {
            database.delete(db_name, "country = ?" + "data = ?" + "temperature = ?", new String[]{currentTemperatureItem.getCountry(), currentTemperatureItem.getDate(), currentTemperatureItem.getTemperature()});
        } catch (Exception e){
            Log.wtf(TAG, e.fillInStackTrace());
        } finally {
            database.close();
        }
        if (database != null){
            this.database.close();
        }
    }


}
