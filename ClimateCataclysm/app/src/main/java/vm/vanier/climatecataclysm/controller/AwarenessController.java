package vm.vanier.climatecataclysm.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import vm.vanier.climatecataclysm.model.AwarenessEntry;

/***
 *  Controller that manages the interactions between a view and the AwarenessEntry model using the awareness table
 *
 * @author Mauran P.
 * @version Version 1
 *
 * References:
 * - Advanced db example by Professor Sleiman
 *  -https://www.javahelps.com/2015/03/advanced-android-database.html
 */
public class AwarenessController {

    /**
     * TAG name for logcat testing
     */
    private static final String TAG = "AwarenessController";
    /**
     * Stores name of ClimateChangeDBAccessController
     */
    private ClimateChangeDBAccessController dbAccessHelper;
    /**
     * SQLiteDatabase variable
     */
    private SQLiteDatabase database;
    /**
     * Database table name
     */
    private String DB_TABLE_NAME = "awareness";
    /**
     * Column name that stores to which climate aspect the entry is related to.
     */
     private static final String COLUMN_ASPECT="entry_aspect";
    /**
     * Column name that stores the text content of the entry
     */
    private static final String COLUMN_RESPONSE="entry_response";
    /**
     * Column name for primary key
     */
    private static final String COLUMN_ID="entry_id";



    public AwarenessController(Context context){
        this.dbAccessHelper = ClimateChangeDBAccessController.getInstance(context);

    }

    public List<AwarenessEntry> getAllAwarenessEntries() {

        List<AwarenessEntry> entryList = new ArrayList<>();

        database = this.dbAccessHelper.openDb();

        try {
            //we want to get all the data from the database, so the cursor must be moved to first
            Cursor cursor = database.rawQuery("SELECT * FROM " + DB_TABLE_NAME, null);
            cursor.moveToFirst();

            //we want to retrieve the information stored in the database about the Awareness Entry and use that to instantiate AwarenessEntry objects that can be added to the list
            while (!cursor.isAfterLast()) {
                AwarenessEntry myEntry = new AwarenessEntry();
                myEntry.setEntryAspect(cursor.getString(cursor.getColumnIndex(COLUMN_ASPECT)));
                myEntry.setEntryResponse(cursor.getString(cursor.getColumnIndex(COLUMN_RESPONSE)));

                entryList.add(myEntry);
                cursor.moveToNext();
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "getAllAwarenessEntries:  catch: " + e.getMessage());
        }

        this.dbAccessHelper.closeDb();
        Log.d(TAG, "getAllAwarenessEntries:  db closed");

        return entryList;
    }

    public void createAwarenessEntry(AwarenessEntry entry) {

        database = this.dbAccessHelper.openDb();


        database.beginTransaction();
        try {
            ContentValues values = new ContentValues();

            values.put(COLUMN_ASPECT, entry.getEntryAspect());
            values.put(COLUMN_RESPONSE, entry.getEntryResponse());

            Cursor cursor = database.rawQuery("SELECT * FROM " + DB_TABLE_NAME, null);

            cursor.moveToLast(); //we add new entries towards the end

            database.insert(DB_TABLE_NAME, null, values);
            Log.d(TAG, "createAwarenessEntry: added entry");

            database.setTransactionSuccessful();
        } catch (Exception e) {
            Log.wtf(TAG, e.fillInStackTrace());
        }
            database.endTransaction();
            this.dbAccessHelper.closeDb();

    }


    public void updateAwarenessEntry( AwarenessEntry oldEntry, AwarenessEntry newEntry) {

        database = this.dbAccessHelper.openDb();

    }


    public void removeAwarenessEntry(AwarenessEntry entry) {

        database = this.dbAccessHelper.openDb();

        try {
            database.delete(DB_TABLE_NAME, COLUMN_RESPONSE + " = \" " + entry.getEntryResponse() + " \" ", null);
        } catch (Exception e) {
            Log.wtf(TAG, e.fillInStackTrace());
        }

        database.close();
        dbAccessHelper.closeDb();
    }



}
