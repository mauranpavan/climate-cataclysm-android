package vm.vanier.climatecataclysm.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import vm.vanier.climatecataclysm.R;
import vm.vanier.climatecataclysm.model.TemperatureItem;
import vm.vanier.climatecataclysm.activities.TemperatureUser;

public class ClimateAdapter extends ArrayAdapter<TemperatureItem> {
    private TemperatureUser temperatureUser;
    private List<TemperatureItem> temperatureItemList;

    public ClimateAdapter(TemperatureUser temperatureUser, Context context, List<TemperatureItem> temperatureItemList){
        super(context, 0, temperatureItemList);
        this.temperatureItemList = temperatureItemList;
        this.temperatureUser = temperatureUser;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = temperatureUser.getLayoutInflater().inflate(R.layout.fragment_temperature_list_item, parent, false);
        }
        TextView txtLocation = (TextView) convertView.findViewById(R.id.txtLocation);
        TextView txtDate = (TextView) convertView.findViewById(R.id.txtDate);
        TextView txtTemperature = (TextView) convertView.findViewById(R.id.txtTemperature);
        TemperatureItem temperatureItem = this.temperatureItemList.get(position);
        txtLocation.setText(temperatureItem.getCountry());
        txtDate.setText(temperatureItem.getDate());
        txtTemperature.setText( temperatureItem.getTemperature());
        return convertView;
    }
}
