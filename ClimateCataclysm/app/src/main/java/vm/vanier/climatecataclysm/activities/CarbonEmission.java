package vm.vanier.climatecataclysm.activities;

import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

import vm.vanier.climatecataclysm.R;
import vm.vanier.climatecataclysm.controller.CarbonController;


/**
 * Displays the Carbon Emission per country annually from the Year 2000 to 2017
 *
 * @author Mauran P.
 * @version Version 3
 *
 */
public class CarbonEmission extends AppCompatActivity {

    private final static String TAG = "CarbonEmission";
    private final static int REQUEST_CODE = 42;
    private String selectedCountry="";
    private Intent intentToCountries;
    private Intent intentToCarbonDisplay;
    private Button btnSelectCountry;
    private Button btnCarbonEmissionSpeak;
    private TextView lblCarbonEmissionDesc1;
    private TextToSpeech textToSpeech;
    private CarbonController carbonController;
    private ArrayList<String> carbonEmissionList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carbon_emission);

        carbonController = new CarbonController(this);

        intentToCountries = new Intent(this, CarbonEmissionCountries.class);
        intentToCarbonDisplay = new Intent(this, CarbonEmissionDisplay.class);

        //initialize ui components
        lblCarbonEmissionDesc1 = findViewById(R.id.lblCarbonEmissionDesc1);
        btnCarbonEmissionSpeak = findViewById(R.id.btnSpeakCarbonEmission);
        btnSelectCountry = findViewById(R.id.btnSelectCountry);

        btnCarbonEmissionSpeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playAudioText(lblCarbonEmissionDesc1.getText().toString());
            }
        });

        btnSelectCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(intentToCountries, REQUEST_CODE);
            }
        });

    }

    private void playAudioText(final String speechStr) {

        textToSpeech = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    textToSpeech.setLanguage(Locale.JAPANESE); //sets the accent
                    textToSpeech.setSpeechRate(0.9f); // 1 would be for normal speech rate

                    textToSpeech.speak(speechStr,TextToSpeech.QUEUE_FLUSH,null,null); //QUEUE_FLUSH will drop the text that was previously being converted

                }
                else
                    Log.e(TAG, "Text to Speech failed.");

            }
        });


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == REQUEST_CODE) {

            if (resultCode == RESULT_OK) {
                Toast.makeText(this, "onActivityResult-if statement resultCode", Toast.LENGTH_SHORT);

                selectedCountry = data.getStringExtra("country");
                Log.d(TAG, "onActivityResult: country is" + selectedCountry);

                carbonEmissionList = (ArrayList<String>) carbonController.getEmissionDataByCountry(selectedCountry);

                Log.d(TAG, "onActivityResult: carbonEmissionList for " + selectedCountry + ":\n " + carbonEmissionList);

                intentToCarbonDisplay.putExtra("country",selectedCountry);
                intentToCarbonDisplay.putStringArrayListExtra("carbonEmissionList", carbonEmissionList);

                startActivity(intentToCarbonDisplay);

            }

        }
    }

    /**
     * Options Menu with StartScreen button
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.climate_home_button_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.opMenuItemHome:
                Toast.makeText(this, "StartScreen menu item clicked", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, StartScreen.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
