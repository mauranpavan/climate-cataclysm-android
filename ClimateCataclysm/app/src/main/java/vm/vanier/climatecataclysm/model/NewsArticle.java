package vm.vanier.climatecataclysm.model;

/**
 * Stores relevant attributes and methods of a news article
 *
 * @author Mauran P
 * @version Version 1
 */
public class NewsArticle {

    private String title;

    //Constructor
    public NewsArticle(String title){

        this.title = title;

    }

    public String getTitle(){
        return title;
    }




}
