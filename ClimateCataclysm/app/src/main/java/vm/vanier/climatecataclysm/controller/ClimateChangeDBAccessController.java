package vm.vanier.climatecataclysm.controller;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Controller that controls the access to the database
 *
 * @author Mauran P.
 * @version Version 1
 *
 * References:
 * - Advanced DB example by Professor Sleiman
 *
 */
public class ClimateChangeDBAccessController {

    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase db;
    private static ClimateChangeDBAccessController instance;

    public ClimateChangeDBAccessController(Context context){

        this.openHelper = new ClimateChangeDatabaseOpenHelper(context);
    }

    public static synchronized ClimateChangeDBAccessController getInstance(Context context) {
        if (instance == null) {
            instance = new ClimateChangeDBAccessController(context);
        }
        return instance;
    }

    /**
     * This will open the database that is requested.
     * @return The SQLiteDatabase requested
     */
    public SQLiteDatabase openDb() {

        this.db = openHelper.getWritableDatabase();
        return this.db;
    }

    /**
     *  This will close the database.
     */
    public void closeDb() {
        if (db != null) {
            this.db.close();
        }
    }


}
