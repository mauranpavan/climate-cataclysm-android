package vm.vanier.climatecataclysm.activities;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

import vm.vanier.climatecataclysm.R;
import vm.vanier.climatecataclysm.adapters.SpreadAwarenessAdapter;
import vm.vanier.climatecataclysm.controller.AwarenessController;
import vm.vanier.climatecataclysm.fragment.AwarenessInputDialog;
import vm.vanier.climatecataclysm.fragment.IAwarenessDialogListener;
import vm.vanier.climatecataclysm.model.AwarenessEntry;

/**
 * Allows the User to give their personal input, thoughts and what they learned on various climate aspects
 *
 * @author Mauran P
 * @version Version 1
 *
 * References:
 *  -Android Notifications: http://thetechnocafe.com/guide-to-notifications-in-android/
 *  -https://developer.android.com/reference/android/app/Notification.Builder.html#addAction(int,%20java.lang.CharSequence,%20android.app.PendingIntent)
 */
public class SpreadAwareness extends AppCompatActivity implements IAwarenessDialogListener {

    private static String TAG = "SpreadAwareness";
    private ArrayList<AwarenessEntry> awarenessEntryList;
    private SpreadAwarenessAdapter adapter;
    private RecyclerView rvSpreadAwareness;
    private Button btnAdd;
    private Button btnDelete;
    private AwarenessEntry currentEntry;
    private AwarenessController awarenessController;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spread_awareness);

        awarenessController = new AwarenessController(getApplicationContext());
        awarenessEntryList = new ArrayList<>();

        //retrieve data from database and add to the arraylist
        for (AwarenessEntry entry: awarenessController.getAllAwarenessEntries() ) {
            awarenessEntryList.add(entry);
        }


        //Locate the recycle view in the activity layout
        rvSpreadAwareness = findViewById(R.id.rvSpreadAwareness);

        adapter = new SpreadAwarenessAdapter(awarenessEntryList);

        rvSpreadAwareness.setAdapter(adapter);
        rvSpreadAwareness.setLayoutManager(new LinearLayoutManager(this));


        //initialize ui components
        btnAdd = findViewById(R.id.btnAddSpreadAwareness);
        btnDelete = findViewById(R.id.btnDeleteSpreadAwareness);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                        startAwarenessInputDialog();
            }
        });
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //deleteEntry();
            }
        });



    }


    @Override
    public void startAwarenessInputDialog() {

        FragmentManager fm = getSupportFragmentManager();
        AwarenessInputDialog inputDialog = new AwarenessInputDialog();
        inputDialog.show(fm, TAG);

    }

    @Override
    public void onFinishAwarenessInputDialog(AwarenessEntry entry) {

        addEntry(entry);
    }

    /**
     * Adds User's response to a climate aspect
     * @param entry
     */
    public void addEntry(AwarenessEntry entry){

        this.awarenessController.createAwarenessEntry(entry);
        this.adapter.notifyDataSetChanged();


    }

    /**
     * Deletes the selected User entry
     */
    public void deleteEntry(AwarenessEntry entry){

        this.awarenessController.removeAwarenessEntry(entry);
        this.adapter.notifyDataSetChanged();

    }

    /**
     * Options Menu with home button
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.climate_home_button_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.opMenuItemHome:
                Toast.makeText(this, "Home menu item clicked", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, StartScreen.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
