package vm.vanier.climatecataclysm.controller;



import android.content.Context;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

/**
 * Creates the climatechange database
 *
 * @author Mauran P
 * @version Version 1
 *
 * References:
 * -https://www.javahelps.com/2015/04/import-and-use-external-database-in.html
 * -https://www.javahelps.com/2015/03/advanced-android-database.html
 */
public class ClimateChangeDatabaseOpenHelper extends SQLiteAssetHelper {


    /**
     * Database name followed with .db file extension to indicate to Android that this is a database.
     */
    private static final String DATABASE_NAME = "climatechange.db";
    /**
     * Indicates the database version number.
     */
    private static final int DATABASE_VERSION = 1;


    public ClimateChangeDatabaseOpenHelper(Context context) {
       super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }









}
