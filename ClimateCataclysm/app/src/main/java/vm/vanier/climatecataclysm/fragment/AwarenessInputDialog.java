package vm.vanier.climatecataclysm.fragment;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import vm.vanier.climatecataclysm.R;
import vm.vanier.climatecataclysm.model.AwarenessEntry;

/**
 * Gets User input to add an awareness entry
 *
 * @author Mauran P
 * @version Version 1
 *
 * References:
 *  -https://www.mkyong.com/android/android-radio-buttons-example/
 */
public class AwarenessInputDialog extends DialogFragment {

    private static String TAG = "AwarenessInputDialog";
    private RadioGroup radioClimateAspectsGroup;
    private RadioButton radioBtnClimateAspects;
    private RadioButton radioBtnTemperature;
    private RadioButton radioBtnPrecipitation;
    private RadioButton radioBtnCarbonEmission;
    private RadioButton radioBtnSeaLevelRise;
    private RadioButton radioBtnClimateAspectOzoneLayer;
    private EditText txtAwarenessEntryResponse;
    private Button btnSubmit;
    private Button btnCancel;

    public AwarenessInputDialog() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_awareness_input_dialog, container, false);

        radioClimateAspectsGroup =  view.findViewById(R.id.radioClimateAspects);
        radioBtnTemperature = view.findViewById(R.id.radioBtnClimateAspectTemperature);
        radioBtnPrecipitation = view.findViewById(R.id.radioBtnClimateAspectPrecipitation);
        radioBtnSeaLevelRise = view.findViewById(R.id.radioBtnClimateAspectSeaLevelRise);
        radioBtnCarbonEmission = view.findViewById(R.id.radioBtnClimateAspectCarbonEmission);
        radioBtnClimateAspectOzoneLayer = view.findViewById(R.id.radioBtnClimateAspectOzoneLayer);

        txtAwarenessEntryResponse = (EditText) view.findViewById(R.id.txtAwarenessEntryResponse);

        btnSubmit = (Button) view.findViewById(R.id.btnSubmitAwarenessInputDialog);
        btnCancel = (Button) view.findViewById(R.id.btnCancelAwarenessInputDialog);

        radioClimateAspectsGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId){
                    case R.id.radioBtnClimateAspectTemperature:
                        Toast.makeText(getContext(), "" + radioBtnTemperature.getText().toString(), Toast.LENGTH_SHORT).show();
                        break;

                    case R.id.radioBtnClimateAspectPrecipitation:
                        Toast.makeText(getContext(), "" + radioBtnPrecipitation.getText().toString(), Toast.LENGTH_SHORT).show();
                        break;

                    case R.id.radioBtnClimateAspectSeaLevelRise:
                        Toast.makeText(getContext(), "" + radioBtnSeaLevelRise.getText().toString(), Toast.LENGTH_SHORT).show();
                        break;

                    case R.id.radioBtnClimateAspectCarbonEmission:
                        Toast.makeText(getContext(), "" + radioBtnCarbonEmission.getText().toString(), Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioBtnClimateAspectOzoneLayer:
                        Toast.makeText(getContext(), "" + radioBtnClimateAspectOzoneLayer.getText().toString(), Toast.LENGTH_SHORT).show();
                        break;

                }
            }
        });



        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AwarenessEntry entry;


                int selectedBtnId = radioClimateAspectsGroup.getCheckedRadioButtonId();

                 switch (selectedBtnId){

                     case R.id.radioBtnClimateAspectTemperature:
                         radioBtnClimateAspects = radioBtnTemperature;
                         Toast.makeText(getContext(), "Submitting " + radioBtnClimateAspects.getText().toString(), Toast.LENGTH_SHORT).show();
                         break;

                     case R.id.radioBtnClimateAspectPrecipitation:
                         radioBtnClimateAspects = radioBtnPrecipitation;
                         Toast.makeText(getContext(), "Submitting " + radioBtnClimateAspects.getText().toString(), Toast.LENGTH_SHORT).show();
                         break;

                     case R.id.radioBtnClimateAspectSeaLevelRise:
                         radioBtnClimateAspects = radioBtnSeaLevelRise;
                         Toast.makeText(getContext(), "Submitting " + radioBtnClimateAspects.getText().toString(), Toast.LENGTH_SHORT).show();
                         break;

                     case R.id.radioBtnClimateAspectCarbonEmission:
                         radioBtnClimateAspects = radioBtnCarbonEmission;
                         Toast.makeText(getContext(), "Submitting " + radioBtnClimateAspects.getText().toString(), Toast.LENGTH_SHORT).show();
                         break;
                     case R.id.radioBtnClimateAspectOzoneLayer:
                         radioBtnClimateAspects = radioBtnClimateAspectOzoneLayer;
                         Toast.makeText(getContext(), "Submitting " + radioBtnClimateAspects.getText().toString(), Toast.LENGTH_SHORT).show();
                         break;

                 }

                entry = new AwarenessEntry(radioBtnClimateAspects.getText().toString(),txtAwarenessEntryResponse.getText().toString());
                 IAwarenessDialogListener activity = (IAwarenessDialogListener) getActivity();

                 //passing the entry object
                 activity.onFinishAwarenessInputDialog(entry);

                Log.d(TAG, "onClick: entry " + entry);
               dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        return view;
    }

}
