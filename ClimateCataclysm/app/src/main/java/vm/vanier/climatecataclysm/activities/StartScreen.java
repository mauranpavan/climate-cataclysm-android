package vm.vanier.climatecataclysm.activities;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.Locale;

import vm.vanier.climatecataclysm.R;
import vm.vanier.climatecataclysm.fragment.IProfileInputDialogListener;
import vm.vanier.climatecataclysm.fragment.ProfileInputDialog;
import vm.vanier.climatecataclysm.model.UserProfile;


/**
 * Serves as splash screen. Intially screen that the User interacts with. Creates the main notification channel here.
 *
 * @author Mauran P.
 * @version Version 1
 *
 * References:
 * - https://developer.android.com/guide/topics/ui/menus#options-menu
 * - https://www.youtube.com/watch?v=oh4YOj9VkVE
 * -Changing localization programmatically: https://sureshjoshi.com/mobile/changing-android-locale-programmatically/
 *  - https://guides.codepath.com/android/Organizing-your-Source-Files
 */
public class StartScreen extends AppCompatActivity implements IProfileInputDialogListener {

    private static final String TAG = "StartScreen";
    private Button btnStart;
    private Intent intent;
    private SharedPreferences sharedPreferences;
    private static final String CHANNEL_ID = "MainChannel";
    private static final int NOTIFICATION_ID=42;
    private  NotificationChannel channel;
    private NotificationManager notificationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //hides action bar
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        sharedPreferences = getSharedPreferences("Settings",MODE_PRIVATE);

        //updates the language
        updateLanguage();


        //Initialize UI components
        btnStart = findViewById(R.id.btnStart);

       if(sharedPreferences.getBoolean("Setup",false)) { //default value is false, when Setup is true (Setup was completed) then we dont need to get user profile info

           intent = new Intent(StartScreen.this, ClimateAspects.class);
           startActivity(intent); //if we already did the app setup, we can immediately go to ClimateAspects activity
       }
       else {// Executes the code below when it is First time setup for the app

           //Instantiate a notification channel
          channel = new NotificationChannel(CHANNEL_ID,"This is a secure channel", NotificationManager.IMPORTANCE_HIGH);

           //Getting an instance of notification manager and creating the notification channel
           notificationManager  = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
           notificationManager.createNotificationChannel(channel);


           btnStart.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {

                   //Get user profile inputs such as country, age, name and language preferences
                   startProfileInputDialog();

               }
           });
       }

    }

    @Override
    public void startProfileInputDialog() {

        FragmentManager fm = getSupportFragmentManager();
        ProfileInputDialog inputDialog = new ProfileInputDialog();
        inputDialog.show(fm, TAG);

    }

    @Override
    public void onFinishProfileInputDialog(UserProfile userProfile, String selectedLanguage) {


        SharedPreferences.Editor sharedEditor = sharedPreferences.edit();

        sharedEditor.putString("Language",selectedLanguage);
        sharedEditor.putString("Name", userProfile.getName());
        sharedEditor.putInt("Age",userProfile.getAge());
        sharedEditor.putString("Country",userProfile.getCountry());
        sharedEditor.putBoolean("Setup", true); //this means we finished setting up the app so the next time the user opens app, they wont see this splash screen


        sharedEditor.apply();

        Log.d(TAG, "onFinishProfileInputDialog: locale " + sharedPreferences.getString("Language","ENGLISH").toLowerCase().substring(0,2) );
        Log.d(TAG, "onFinishProfileInputDialog: boolean" +  sharedPreferences.getString("Language","ENGLISH").toLowerCase().substring(0,2).equals("fr"));

        updateLanguage();

        intent = new Intent(StartScreen.this, ClimateAspects.class);
        startActivity(intent);
    }

    public void updateLanguage(){


        //updates the language
        Locale locale = new Locale(sharedPreferences.getString("Language","ENGLISH").toLowerCase().substring(0,2)) ; //Locale.FRENCH.getLanguage() will return fr
        Locale.setDefault(locale);


        Resources resources = getBaseContext().getResources();

        Configuration configuration = resources.getConfiguration();
        configuration.setLocale(locale); //since configuration.locale is deprecated, we use setLocale
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());

        /*
        Context context = getApplication().getBaseContext();
        context.createConfigurationContext(configuration);
        */

    }






}
