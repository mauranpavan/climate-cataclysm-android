package vm.vanier.climatecataclysm.fragment;

import vm.vanier.climatecataclysm.model.UserProfile;


/**
 * Interface associated with getting User Information in StartScreen Activity
 *
 * @author Mauran P.
 * @version Version 2
 */
public interface IProfileInputDialogListener {

    void startProfileInputDialog();
    void onFinishProfileInputDialog(UserProfile userProfile, String selectedLanguage);
}
