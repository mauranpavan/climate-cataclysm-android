package vm.vanier.climatecataclysm.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

import vm.vanier.climatecataclysm.R;
import vm.vanier.climatecataclysm.controller.PrecipitationAPIController;
import vm.vanier.climatecataclysm.model.MonthlyAveragePrecipitation;

/**
 * Displays information about monthly average precipitation within specified fixed year intervals
 *
 * @author Mauran P.
 * @version Version 3
 *
 * References:
 * - https://github.com/jjoe64/GraphView/wiki/Bar-Graph
 * - Precipitation API: https://datahelpdesk.worldbank.org/knowledgebase/articles/902061-climate-data-api
 * -https://docs.oracle.com/javase/7/docs/api/java/text/DecimalFormat.html#format(double,%20java.lang.StringBuffer,%20java.text.FieldPosition)
 * -Country codes: https://unstats.un.org/unsd/methodology/m49/
 * - https://android.okhelp.cz/get-resource-id-by-resources-string-name-android-example/
 * - https://stackoverflow.com/questions/13351003/find-drawable-by-string
 */
public class Precipitation extends AppCompatActivity {

    private static final String TAG = "Precipitation";
    private TextView lblCountryNamePrec;
    private Button btnSelectCountry;
    private Button btnSaveChanges;
    private ImageView imgViewCountryFlag;
    private RadioGroup radioGroupPrecipitationYearInterval;
    private RadioButton radioBtnPrecYearInterval1920;
    private RadioButton radioBtnPrecYearInterval1940;
    private RadioButton radioBtnPrecYearInterval1960;
    private RadioButton radioBtnPrecYearInterval1980;
    private RadioButton radioBtnPrecYearInterval2020;
    private RadioButton radioBtnPrecYearInterval2040;
    private RadioButton radioBtnPrecYearInterval2060;
    private RadioButton radioBtnPrecYearInterval2080;

    private String urlStr= "http://climatedataapi.worldbank.org/climateweb/rest/v1/country/mavg/inmcm3_0/pr/";
    private String fromYear="1920";
    private String toYear="1939";
    private String countryCode="CAN";
    private GraphView graph;
    private BarGraphSeries<DataPoint> series;
    private static final String DEFAULT_FROM_YEAR="1920";
    private static final String DEFAULT_TO_YEAR="1939";
    private static final String DEFAULT_COUNTRY_CODE="CAN";
    private static final String DEFAULT_FLAG_NAME ="canada";
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor sharedEditor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_precipitation);

        sharedPreferences = getPreferences(MODE_PRIVATE);
        sharedEditor = sharedPreferences.edit();

        fromYear = sharedPreferences.getString("fromYear",DEFAULT_FROM_YEAR); //When the User starts this activty for the first time, the sharedPreferences value are empty so we would use our default values
        toYear = sharedPreferences.getString("toYear",DEFAULT_TO_YEAR);
        countryCode = sharedPreferences.getString("countryCode",DEFAULT_COUNTRY_CODE);


        sharedEditor.apply();

        //initialize ui components
        lblCountryNamePrec = findViewById(R.id.lblCountryNamePrec);
        btnSelectCountry = findViewById(R.id.btnSelectCountryPrecipitation);
        btnSaveChanges = findViewById(R.id.btnSaveChanges);
        imgViewCountryFlag = findViewById(R.id.imgViewPrecCountryFlag);
        radioGroupPrecipitationYearInterval = findViewById(R.id.radioGroupPrecipitationYearInterval);
        radioBtnPrecYearInterval1920 = findViewById(R.id.radioBtnPrecYearInterval1920);
        radioBtnPrecYearInterval1940 = findViewById(R.id.radioBtnPrecYearInterval1940);
        radioBtnPrecYearInterval1960 = findViewById(R.id.radioBtnPrecYearInterval1960);
        radioBtnPrecYearInterval1980 = findViewById(R.id.radioBtnPrecYearInterval1980);
        radioBtnPrecYearInterval2020 = findViewById(R.id.radioBtnPrecYearInterval2020);
        radioBtnPrecYearInterval2040 = findViewById(R.id.radioBtnPrecYearInterval2040);
        radioBtnPrecYearInterval2060 = findViewById(R.id.radioBtnPrecYearInterval2060);
        radioBtnPrecYearInterval2080 = findViewById(R.id.radioBtnPrecYearInterval2080);
        graph = findViewById(R.id.precipitationGraph);

        //set the flag for the currently selected country and set the country name
        setFlag(sharedPreferences.getString("flagName",DEFAULT_FLAG_NAME));
        lblCountryNamePrec.setText(sharedPreferences.getString("flagName",DEFAULT_FLAG_NAME).toUpperCase());

        //since we are loading saved year interval from sharedpreferences, we need to show it by setting the correponding radiobutton
        radioGroupPrecipitationYearInterval.clearCheck();
        switch (fromYear){
            case "1920":
                radioBtnPrecYearInterval1920.setChecked(true);
                break;
            case "1940":
                radioBtnPrecYearInterval1940.setChecked(true);
                break;
            case "1960":
                radioBtnPrecYearInterval1960.setChecked(true);
                break;
            case "1980":
                radioBtnPrecYearInterval1980.setChecked(true);
                break;
            case "2020":
                radioBtnPrecYearInterval2020.setChecked(true);
                break;
            case "2040":
                radioBtnPrecYearInterval2040.setChecked(true);
                break;
            case "2060":
                radioBtnPrecYearInterval2060.setChecked(true);
                break;
            case "2080":
                radioBtnPrecYearInterval2080.setChecked(true);
                break;
        }

        radioGroupPrecipitationYearInterval.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId){
                    case R.id.radioBtnPrecYearInterval1920:
                        Toast.makeText(getApplicationContext(), "" + radioBtnPrecYearInterval1920.getText().toString(), Toast.LENGTH_SHORT).show();
                        fromYear = "1920";
                        toYear="1939";
                        break;
                    case R.id.radioBtnPrecYearInterval1940:
                        Toast.makeText(getApplicationContext(), "" + radioBtnPrecYearInterval1940.getText().toString(), Toast.LENGTH_SHORT).show();
                        fromYear = "1940";
                        toYear="1959";
                        break;
                    case R.id.radioBtnPrecYearInterval1960:
                        Toast.makeText(getApplicationContext(), "" + radioBtnPrecYearInterval1960.getText().toString(), Toast.LENGTH_SHORT).show();
                        fromYear = "1960";
                        toYear="1979";
                        break;
                    case R.id.radioBtnPrecYearInterval1980:
                        Toast.makeText(getApplicationContext(), "" + radioBtnPrecYearInterval1980.getText().toString(), Toast.LENGTH_SHORT).show();
                        fromYear = "1980";
                        toYear="1999";
                        break;
                    case R.id.radioBtnPrecYearInterval2020:
                        Toast.makeText(getApplicationContext(), "" + radioBtnPrecYearInterval2020.getText().toString(), Toast.LENGTH_SHORT).show();
                        fromYear = "2020";
                        toYear="2039";
                        break;
                    case R.id.radioBtnPrecYearInterval2040:
                        Toast.makeText(getApplicationContext(), "" + radioBtnPrecYearInterval2040.getText().toString(), Toast.LENGTH_SHORT).show();
                        fromYear = "2040";
                        toYear="2059";
                        break;
                    case R.id.radioBtnPrecYearInterval2060:
                        Toast.makeText(getApplicationContext(), "" + radioBtnPrecYearInterval2060.getText().toString(), Toast.LENGTH_SHORT).show();
                        fromYear = "2060";
                        toYear="2079";
                        break;
                    case R.id.radioBtnPrecYearInterval2080:
                        Toast.makeText(getApplicationContext(), "" + radioBtnPrecYearInterval2080.getText().toString(), Toast.LENGTH_SHORT).show();
                        fromYear = "2080";
                        toYear="2099";
                        break;

                }

                sharedEditor.putString("fromYear",fromYear);
                sharedEditor.putString("toYear",toYear);
                sharedEditor.apply();
            }
        });

        registerForContextMenu(btnSelectCountry);


        customizeUrlStr();
        runAsyncTask();

        btnSaveChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
               Intent intent = new Intent(getApplicationContext(), Precipitation.class);
               startActivity(intent);
            }
        });



    }

    public String customizeUrlStr(){



        urlStr += fromYear+ "/" +toYear + "/" + countryCode;
        Log.d(TAG, "customizeUrlStr: " + urlStr);
        return urlStr;

    }

    private void runAsyncTask() {

        PrecipitationAPIAsyncTask apiTask = new PrecipitationAPIAsyncTask();
        apiTask.execute(urlStr);
    }



    public void plotBarGraph(ArrayList<Double> monthlyAvgList){

        int monthIndex;
        ArrayList<DataPoint> coordinatesList= new ArrayList<>();
        DataPoint[] coordinates;

        //Convert our double arrayList into DataPoint arrayList
        for(int i = 0; i <monthlyAvgList.size(); i++){
            monthIndex = i +1; //since our x-axis is for months, we will use 1 to 12 as month index to represent month value. The size of monthlyAvgList is always 12

            //Instantiate DataPoint
            DataPoint point = new DataPoint(monthIndex, monthlyAvgList.get(i));
            coordinatesList.add(point); // we will pass coordinatesList as parameter to create our BarGraphSeries
            monthIndex++; //incrementing monthIndex
        }

        Log.d(TAG, "plotBarGraph: coordinatesList" + coordinatesList);

        //Convert DataPoint arraylist into DataPoint array
        coordinates = new DataPoint[coordinatesList.size()];
        coordinates = coordinatesList.toArray(coordinates);

        series = new BarGraphSeries<>( coordinates);

        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMaxX(13);
        graph.getViewport().setScrollable(true); //enables horizontal scrolling

        graph.addSeries(series);

        //Bar Graph styling and labels
        series.setSpacing(8);  //In order to have spacing between the bars in our bar graph
        series.setColor(R.color.colorLightGreen); //bar color
        series.setDrawValuesOnTop(true); //this will display the value on top of the bar
        series.setValuesOnTopColor(Color.GREEN);
        //setting labels for axis
        graph.getGridLabelRenderer().setHorizontalAxisTitle(getString(R.string.precipitationXAxisLabel));
        graph.getGridLabelRenderer().setVerticalAxisTitle(getString(R.string.precipitationYAxisLabel));
        graph.getGridLabelRenderer().setHorizontalAxisTitleColor(Color.WHITE);
        graph.getGridLabelRenderer().setVerticalAxisTitleColor(Color.WHITE);
        graph.getGridLabelRenderer().setHorizontalLabelsColor(Color.WHITE);
        graph.getGridLabelRenderer().setVerticalLabelsColor(Color.WHITE);
        graph.getGridLabelRenderer().setLabelsSpace(8);

    }

    private void setFlag(String countryName) {
        String originalCountryName = countryName;
        lblCountryNamePrec.setText(countryName.toUpperCase());

        //some countries such as united kingdom and united states of america have whitespace in their name, so we need to replace them with _
        countryName = countryName.replaceAll("\\s","_");

        String flagName = "flag_" + countryName;

        Log.d(TAG, "setFlag: flagName: " + flagName);

        int drawableId = getResources().getIdentifier(flagName, "drawable", getApplicationContext().getPackageName());

        imgViewCountryFlag.setImageResource(drawableId);

        //save the flagName so that we can reloaded
        sharedEditor.putString("flagName",originalCountryName);




    }

    @Override
    /**
     * When a long-click event occurs in the registered view, the system calls this method
     */
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);


        getMenuInflater().inflate(R.menu.country_list_menu, menu);


    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {


        switch(item.getItemId()){
            case R.id.menuItemAlgeria:
                Toast.makeText(this, item.getTitle(),Toast.LENGTH_SHORT).show();
                countryCode = "DZA";
                setFlag(item.getTitle().toString().toLowerCase());
                sharedEditor.putString("countryCode",countryCode);
                sharedEditor.apply();
                return true;
            case R.id.menuItemAustralia:
                Toast.makeText(this, item.getTitle(),Toast.LENGTH_SHORT).show();
                countryCode = "AUS";
                setFlag(item.getTitle().toString().toLowerCase());
                sharedEditor.putString("countryCode",countryCode);
                sharedEditor.apply();
                return true;
            case R.id.menuItemBrazil:
                Toast.makeText(this, item.getTitle(),Toast.LENGTH_SHORT).show();
                countryCode = "BRA";
                setFlag(item.getTitle().toString().toLowerCase());
                sharedEditor.putString("countryCode",countryCode);
                sharedEditor.apply();
                return true;
            case R.id.menuItemCanada:
                Toast.makeText(this, item.getTitle(),Toast.LENGTH_SHORT).show();
                countryCode = "CAN";
                setFlag(item.getTitle().toString().toLowerCase());
                sharedEditor.putString("countryCode",countryCode);
                sharedEditor.apply();
                return true;
            case R.id.menuItemChina:
                Toast.makeText(this, item.getTitle(),Toast.LENGTH_SHORT).show();
                countryCode = "CHN";
                setFlag(item.getTitle().toString().toLowerCase());
                sharedEditor.putString("countryCode",countryCode);
                sharedEditor.apply();
                return true;
            case R.id.menuItemFrance:
                Toast.makeText(this, item.getTitle(),Toast.LENGTH_SHORT).show();
                countryCode = "FRA";
                setFlag(item.getTitle().toString().toLowerCase());
                sharedEditor.putString("countryCode",countryCode);
                sharedEditor.apply();
                return true;
            case R.id.menuItemIndia:
                Toast.makeText(this, item.getTitle(),Toast.LENGTH_SHORT).show();
                countryCode = "IND";
                setFlag(item.getTitle().toString().toLowerCase());
                sharedEditor.putString("countryCode",countryCode);
                sharedEditor.apply();
                return true;
            case R.id.menuItemIran:
                Toast.makeText(this, item.getTitle(),Toast.LENGTH_SHORT).show();
                countryCode = "IRN";
                setFlag(item.getTitle().toString().toLowerCase());
                sharedEditor.putString("countryCode",countryCode);
                sharedEditor.apply();
                return true;
            case R.id.menuItemJapan:
                Toast.makeText(this, item.getTitle(),Toast.LENGTH_SHORT).show();
                countryCode = "JPN";
                setFlag(item.getTitle().toString().toLowerCase());
                sharedEditor.putString("countryCode",countryCode);
                sharedEditor.apply();
                return true;
            case R.id.menuItemMexico:
                Toast.makeText(this, item.getTitle(),Toast.LENGTH_SHORT).show();
                countryCode = "MEX";
                setFlag(item.getTitle().toString().toLowerCase());
                sharedEditor.putString("countryCode",countryCode);
                sharedEditor.apply();
                return true;
            case R.id.menuItemUnitedKingdom:
                Toast.makeText(this, item.getTitle(),Toast.LENGTH_SHORT).show();
                countryCode = "GBR";
                setFlag(item.getTitle().toString().toLowerCase());
                sharedEditor.putString("countryCode",countryCode);
                sharedEditor.apply();
                return true;
            case R.id.menuItemUnitedStatesOfAmerica:
                Toast.makeText(this, item.getTitle(),Toast.LENGTH_SHORT).show();
                countryCode = "USA";
                setFlag(item.getTitle().toString().toLowerCase());
                sharedEditor.putString("countryCode",countryCode);
                sharedEditor.apply();
                return true;

            default:
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
                return super.onContextItemSelected(item);
        }

    }


    private class PrecipitationAPIAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            String response = "";
            PrecipitationAPIController apiController = new PrecipitationAPIController();

            response = apiController.callAPI(strings[0]);
            Log.i(TAG, response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            //Instantiate MonthlyAveragePrecipitation
            MonthlyAveragePrecipitation monthlyAveragePrecipitation = new MonthlyAveragePrecipitation();

            //test parse the JSON response
            try {
                JSONArray resultArr = new JSONArray(result);
                JSONObject precipitationObj = resultArr.getJSONObject(0);

                monthlyAveragePrecipitation.setFromYear(precipitationObj.getString("fromYear"));
                monthlyAveragePrecipitation.setToYear(precipitationObj.getString("toYear"));

                //retrieve the inner JSONArray monthVals
                JSONArray monthValueArr = precipitationObj.getJSONArray("monthVals");

                DecimalFormat decimalFormat = new DecimalFormat("####.#");
                //get the values from the JSONArray and add them into the arraylist of the MonthlyAveragePrecipitation object
                for(int i = 0; i <monthValueArr.length(); i++){
                    monthlyAveragePrecipitation.getMonthlyAvgList().add( Double.parseDouble(decimalFormat.format(monthValueArr.get(i)))); // format method formats the monthValueArr.get(i) into string

                }
                Log.d(TAG, "onPostExecute: monthVals array" + precipitationObj.get("monthVals"));

                Log.d(TAG, "onPostExecute: monthlyAveragePrecipitation: " + monthlyAveragePrecipitation);

                plotBarGraph(monthlyAveragePrecipitation.getMonthlyAvgList());

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }

    /**
     * Options Menu with StartScreen button
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.climate_home_button_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.opMenuItemHome:
                Toast.makeText(this, "StartScreen menu item clicked", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, StartScreen.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
