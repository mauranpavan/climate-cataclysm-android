package vm.vanier.climatecataclysm.model;

/**
 * Profile Object that stores User information that could be needed for the app
 *
 * @author Mauran P.
 * @Version Version 2
 */
public class UserProfile {


    /**
     * Stores User's name
     */
    private String name;
    /**
     * Stores the current country the User resides in
     */
    private String country;
    /**
     * Stores User's age
     */
    private int age;

    public UserProfile(String name,  int age, String country){

        this.name = name;
        this.country = country;
        this.age = age;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


}
