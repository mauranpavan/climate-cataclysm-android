
**Application Description**

This application’s aim is to provide awareness of the rapid climate to the User and encourage them to spread awareness about climate change to others. The app provides information to the User about various climate change aspects such as Temperature, Precipitation, Carbon Emission, Ozone Layer and Sea Level Rise. 

Users are able to assess the climate changes by examining climate change data and images. Climate Cataclysm allow Users to compare climate change data across time or among other countries. Some of climate change aspects data are retrieved from simulation models, and this makes future predictions available to the User as well. Climate Cataclysm app provides the features that allow the User to retain what they have learned or their thoughts on a particular climate aspect (SpreadAwareness feature). By doing so, the User is able to be more comfortable with these topics which would allow them to be more fluent when they have to explain about climate change to others.

Understanding the seriousness of climate change is the first step before taking the necessary actions to protect our planet Earth.

**List of Proposed Features**


- [x] 1. Precipitation Display: 
User can select a country and then view the precipitations for a given year interval. For example, viewing the precipitations for Canada during the year interval 1920-1939. Precipitations can be monthly or yearly values. This can be done also with precipitations.
- [x] 2. Temperature Display: 
Will display the temperature so any country the User selects. This can give the User indication how serious global warming is just by examining the temperature rise.
- [ ] 3. Climate Change Now:
 This will be a documentation section which will provide recommended news articles based on User’s location on climate change events happening in their country. Another tab will show the current news events around the world. This will also have a search bar that the User can use to explore specific global warming topics(frequency of tsunamis or earthquakes). This is where we will use the News API.
- [ ] 4. Extinct Animals:
 Allows the User to view a list of extinct animals using an extinction status (isExtinct=true). A learn more button could direct the User to Youtube player.
- [x] 5. Ozone Layer Indicator: 
This will allow the User to see the thickness of the Ozone layer at their current location. User could also input past year and be able to compare the change. We will also display the dangers at lower ozone level. A learn more button could direct user to a web search or article/news search. Open Weather Map API will be used here. Using JSON.
- [x] 6. Spread Awareness: 
This is a feature that would encourage the User to spread awareness of climate change. The app would provide a template and the user could write what they have retained from using this app. The User will write for each climate change aspect such as temperature, carbon emission, animal extinction, ozone layer, sea level rise. SQLite will be used here to store what the User writes for each aspect.
- [ ] 7. Share: 
The User could send what they learned as a pdf by email to their contacts. This would help everyone acknowledge what they know about climate change and what they should learn. This might be possible with PdfDocument and PrintedPdfDocument classes. The User can send by climate change topic(carbon emission or temperature rise for example).
- [x] 8. Carbon Emission: 
Using a database, we would store carbon emission data for the majority of countries. This will be then displayed as a graph per country selected by the User.
- [x] 9. Sea Level Rise Display: 
Will allow the User to see the sea level rise across years until 2015. User can select a year interval to see when the change was becoming drastic.
- [ ] 10. Sea Level Rise Simulation:
 Would explain the consequences of Sea level rise and show the potential sea level rise in their region(country). User can input a number for the sea level rise(hypothetical) and see if they will be safe. This will use the User’s location elevation.
- [ ] 11. Doomsday Preview:  
This feature is a quick summary feature. It would be show future predictions for some attributes and current data for others. User will select a country and see the related details. The min and max temperature for the future (year 2046 to 2065) will be shown to the User. This would include Current thickness of ozone layer, carbon emission for the country the User is residing, the precipitation for that country.

